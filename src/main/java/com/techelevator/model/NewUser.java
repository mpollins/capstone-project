package com.techelevator.model;

public class NewUser {
	
	public enum Permission {
		STAFF,
		STUDENT,
		EMPLOYER
	}
	
	private String userName;
	private String password;
	private String email;
	private String businessName;
	private String firstName;
	private String lastName;
	private int cohortId;
	private Permission permission;
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getCohortId() {
		return cohortId;
	}
	public void setCohortId(int cohortId) {
		this.cohortId = cohortId;
	}
	public int getPermission() {
		return permission.ordinal();
	}
	public void setPermission(int permission) {
		this.permission = Permission.values()[permission];
	}
	public void setPermission(Permission permission) {
		this.permission = permission;
	}
}
