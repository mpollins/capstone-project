package com.techelevator.model;

import java.util.List;

public class Student {
	private int studentId;
	private int userId;
	private Contact contact;
	private Cohort cohort;
	private boolean isDiscoverable;
	private String interests;
	private String firstName;
	private String lastName;
	private String summary;
	private List<Education> education;
	private List<String> techSkills;
	private List<String> softSkills;
	private List<Experience> experiences;
	private List<Portfolio> portfolio;
	private List<SocialMedia> socialMedia;
	
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public Cohort getCohort() {
		return cohort;
	}
	public void setCohort(Cohort cohort) {
		this.cohort = cohort;
	}
	public boolean isDiscoverable() {
		return isDiscoverable;
	}
	public void setDiscoverable(boolean isDiscoverable) {
		this.isDiscoverable = isDiscoverable;
	}
	public String getInterests() {
		return interests;
	}
	public void setInterests(String interests) {
		this.interests = interests;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public List<String> getTechSkills() {
		return techSkills;
	}
	public void setTechSkills(List<String> techSkills) {
		this.techSkills = techSkills;
	}
	public List<String> getSoftSkills() {
		return softSkills;
	}
	public void setSoftSkills(List<String> softSkills) {
		this.softSkills = softSkills;
	}
	public List<Experience> getExperiences() {
		return experiences;
	}
	public void setExperiences(List<Experience> experiences) {
		this.experiences = experiences;
	}
	public List<Education> getEducation() {
		return education;
	}
	public void setEducation(List<Education> education) {
		this.education = education;
	}
	public List<Portfolio> getPortfolio() {
		return portfolio;
	}
	public void setPorfolio(List<Portfolio> portfolio) {
		this.portfolio = portfolio;
	}
	public List<SocialMedia> getSocialMedia() {
		return socialMedia;
	}
	public void setSocialMedia(List<SocialMedia> socialMedia) {
		this.socialMedia = socialMedia;
	}
}
