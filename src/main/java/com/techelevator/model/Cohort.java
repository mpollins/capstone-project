package com.techelevator.model;

public class Cohort {
	private String location;
	private String programmingLang;
	private int cohortId;
	private int cohortNumber;
	private boolean isCurrent;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getProgrammingLang() {
		return programmingLang;
	}
	public void setProgrammingLang(String programmingLang) {
		this.programmingLang = programmingLang;
	}
	public int getCohortId() {
		return cohortId;
	}
	public void setCohortId(int cohortId) {
		this.cohortId = cohortId;
	}
	public int getCohortNumber() {
		return cohortNumber;
	}
	public void setCohortNumber(int cohortNumber) {
		this.cohortNumber = cohortNumber;
	}
	public boolean isCurrent() {
		return isCurrent;
	}
	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

}
