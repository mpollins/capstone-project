package com.techelevator.model;

import java.util.List;

public interface UserDAO {
	
	public List<Student> getPublicStudents();
	
	public List<Student> getPublicStudents(String location);
	
	public List<Student> getPublicStudents(String location, boolean current);
	
	public boolean searchForUsernameAndPassword(String userName, String password);

	public void updatePassword(String username, String password);

	public Student getStudentById(int studentId);

	public User getUserByUserName(String userName);

	public void addNewStaff(NewUser newUser);

	public List<Cohort> getCohorts();

	public void addNewStudent(NewUser newUser);

	public void addNewEmployer(NewUser newUser);

	public Student getStudentByUserId(int userId);

	public void updateStudent(Student student);

	public void updateEducation(Education education);

	public void addNewEducation(Education education);

	public void addStudentTechSkill(int studentId, String techSkill);

	public void deleteEducation(Education education);

	public void deleteStudentTechSkill(int studentId, String techSkill);

	public void deleteStudentSoftSkill(int studentId, String softSkill);

	public void addStudentSoftSkill(int studentId, String softSkill);

	public void addNewEexperience(Experience experience);

	public void updateExperience(Experience education);

	public void deleteExperience(Experience experience);

	public void addStudentTechSkills(int studentId, List<String> techSkills);

	public void addStudentSoftSkills(int studentId, List<String> softSkills);

	public void deleteStudentTechSkills(int studentId, List<String> techSkills);
	
	public void deleteStudentSoftSkills(int studentId, List<String> softSkills);
	
	public List<String> getAllTechnicalSkills();

	public List<String> getAllSoftSkills();

	public void addNewSocialMedia(int studentId, SocialMedia socialMedia);

	public void deletePortfolio(Portfolio portfolio);

	public void deleteStudentSocialMedia(int studentId, SocialMedia socialMedia);

	public void deleteStudentSocialMedias(int studentId, List<SocialMedia> socialMedias);

	public void updatePortfolio(Portfolio portfolio);

	public void addNewPortfolio(Portfolio portfolio);

	public List<Employer> getAllEmployers();

	public List<User> getAllUsers();

	public String getUserSalt(String userName);

	public String getUserHash(String userName);

	public List<Staff> getAllStaff();

	public List<String> getAllSocialMedia();

	public void updateSetUserActive(User user);

	public void updateSetUserInactive(User user);

	public void updateSetUserActive(String salt);

	public Staff getStaffByUserId(int userId);

	public Employer getEmployerByUserId(int userId);

	public void updateEmployer(Employer employer);

	public void updateStaff(Staff staff);

	public User getUserByUserId(int userId);

	public void addNewCohort(Cohort cohort);

	public void updateSocialMedia(SocialMedia socialMedia, int studentId);

	public void addNewSocialMedia(int studentId, String name, String link);

	public List<Student> getSearchableStudents(String city);

	public List<Student> getSearchableStudentsByLanguage(String language);

	public void deleteEmployer(Employer employer);

	public void deleteEducations(List<Education> educations);

	public List<SocialMedia> getSocialMediaNameId();

	public void deleteExperience(List<Experience> experiences);

	public void deletePortfolio(List<Portfolio> projects);

	public void deleteStudent(Student student);

	public void deleteStaff(Staff staff);

	public void deleteCohort(Cohort cohort);

	public void addNewTechSkill(String techSkill);

	public void deleteTechSkill(String techSkill);
	
}
