package com.techelevator.model;

public class TechSkill {
	private int techSkillId;
	private String techSkillName;
	
	public int getTechSkillId() {
		return techSkillId;
	}
	public void setTechSkillId(int techSkillId) {
		this.techSkillId = techSkillId;
	}
	public String getTechSkillName() {
		return techSkillName;
	}
	public void setTechSkillName(String techSkillName) {
		this.techSkillName = techSkillName;
	}
}
