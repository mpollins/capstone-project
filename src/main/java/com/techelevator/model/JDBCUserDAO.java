package com.techelevator.model;

import java.sql.SQLClientInfoException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.ExemptionMechanism;
import javax.sql.DataSource;

import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.security.PasswordHasher;

@Component
public class JDBCUserDAO implements UserDAO {
	
	private JdbcTemplate jdbcTemplate; 
	private PasswordHasher passwordHasher;
	
	@Autowired
	public JDBCUserDAO(DataSource dataSource, PasswordHasher passwordHasher) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.passwordHasher = passwordHasher;
	}
	
	//***********************************************************
	//******************** Get Methods **************************
	//***********************************************************
	
	@Override
	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM appuser");
		
		while(results.next()) {
			users.add(mapRowToUser(results));
		}
		return users;
	}
	
	@Override
	public User getUserByUserName(String userName) {
		User user = new User();
		String sqlSelectUserByUserName = "SELECT * FROM appuser WHERE userName = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectUserByUserName, userName);
		
		if(results.next()) {
			user = mapRowToUser(results);
		}
		return user;
	}
	
	@Override
	public User getUserByUserId(int userId) {
		User user = new User();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM appuser WHERE userId = ?", userId);
		
		if(results.next()) {
			user = mapRowToUser(results);
		}
		return user;
	}
	
	@Override
	public List<Cohort> getCohorts() {
		List<Cohort> cohorts = new ArrayList<>();
		String sqlSelectAllCohorts = "SELECT * FROM cohort";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectAllCohorts);
		
		while(results.next()) {
			cohorts.add(mapRowToCohort(results));
		}
		return cohorts;
	}

	@Override
	public List<Student> getPublicStudents() {
		List<Student> students = new ArrayList<>();
		String sqlSelectPublicStudentInfo = "SELECT * FROM student ORDER BY lastName";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectPublicStudentInfo);
		
		while(results.next()) {
			students.add(mapRowToStudent(results));
		}
		return students;
	}
	
	@Override
	public List<Student> getPublicStudents(String location) {
		List<Student> students = new ArrayList<>();
		String sqlSelectPublicStudentInfo = "SELECT * FROM student INNER JOIN cohort ON student.cohortId = cohort.cohortId " +
											"WHERE location = ? ORDER BY lastName";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectPublicStudentInfo, location);
		
		while(results.next()) {
			students.add(mapRowToStudent(results));
		}
		return students;
	}
	
	@Override
	public List<Student> getPublicStudents(String location, boolean current) {
		List<Student> students = new ArrayList<>();
		String sqlSelectPublicStudentInfo = "SELECT * FROM student INNER JOIN cohort ON student.cohortId = cohort.cohortId " +
											"WHERE location = ? AND isCurrent = ? ORDER BY lastName";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectPublicStudentInfo, location, current);
		
		while(results.next()) {
			students.add(mapRowToStudent(results));
		}
		return students;
	}
	
	@Override
	public List<Student> getSearchableStudentsByLanguage(String language) {
		List<Student> students = new ArrayList<>();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM student s INNER JOIN cohort c ON s.cohortId = c.cohortId " +
														"WHERE s.isDiscoverable = true AND c.programmingLang = ? ORDER BY lastName", 
														language);
		while(results.next()) {
			students.add(mapRowToStudent(results));
		}
		return students;
	}
	
	@Override
	public List<Student> getSearchableStudents(String city) {
		List<Student> students = new ArrayList<>();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM student s INNER JOIN cohort c ON s.cohortId = c.cohortId " + 
														"WHERE s.isDiscoverable = true AND c.location = ? ORDER BY lastName",
														city);
		while(results.next()) {
			students.add(mapRowToStudent(results));
		}
		return students;
	}
	
	@Override
	public List<Employer> getAllEmployers() {
		List<Employer> employers = new ArrayList<>();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM employer");
		
		while(results.next()) {
			employers.add(mapRowToEmployer(results));
		}
		return employers;
	}
	
	@Override
	public Employer getEmployerByUserId(int userId) {
		Employer employer = new Employer();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM employer WHERE userId = ?", userId);
		
		if(results.next()) {
			employer = mapRowToEmployer(results);
		}
		return employer;
	}
	
	@Override
	public List<Staff> getAllStaff() {
		List<Staff> staff = new ArrayList<>();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM staff");
		
		while(results.next()) {
			staff.add(mapRowToStaff(results));
		}
		return staff;
	}
	
	@Override
	public Staff getStaffByUserId(int userId) {
		Staff staff = new Staff();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM staff WHERE userId = ?", userId);
		
		if(results.next()) {
			staff = mapRowToStaff(results);
		}
		return staff;
	}
	
	@Override
	public Student getStudentById(int studentId) {
		Student student = new Student();
		String sqlSelectStudentById = "SELECT * FROM student WHERE studentId = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectStudentById, studentId);
		
		if(results.next()) {
			student = mapRowToStudent(results);
		}
		return student;
	}
	
	@Override
	public Student getStudentByUserId(int userId) {
		Student student = new Student();
		String sqlSelectStudentById = "SELECT * FROM student WHERE userId = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectStudentById, userId);
		
		if(results.next()) {
			student = mapRowToStudent(results);
		}
		return student;
	}
	
	private List<String> getStudentTechSkills(int studentId) {
		List<String> techSkills = new ArrayList<>();
		String sqlGetTechSkills = "SELECT techSkillName FROM tech_skills " +
								  "INNER JOIN student_tech_skills sts ON tech_skills.techSkillId = sts.techSkillId " +
								  "WHERE studentID = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetTechSkills, studentId);
		
		while(results.next()) {
			techSkills.add(results.getString("techSkillName"));
		}
		return techSkills;
	}
	
	private List<String> getStudentSoftSkills(int studentId) {
		List<String> softSkills = new ArrayList<>();
		String sqlGetsoftSkills = "SELECT softSkillName FROM soft_skills " +
								  "INNER JOIN student_soft_skills sss ON soft_skills.softSkillId = sss.softSkillId " +
								  "WHERE studentID = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetsoftSkills, studentId);
		
		while(results.next()) {
			softSkills.add(results.getString("softSkillName"));
		}
		return softSkills;
	}
	
	private List<Experience> getStudentExperience(int studentId) {
		List<Experience> experiences = new ArrayList<>();
		String sqlGetExperience = "SELECT * FROM experience WHERE studentId = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetExperience, studentId);
		
		while(results.next()) {
			experiences.add(mapRowToExperience(results));
		}
		return experiences;
	}
	
	private List<Education> getStudentEducation(int studentId) {
		List<Education> education = new ArrayList<>();
		String sqlGetEducation = "SELECT * FROM education WHERE studentId = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetEducation, studentId);
		
		while(results.next()) {
			education.add(mapRowToEducation(results));
		}
		return education;
	}
	
	private List<Portfolio> getStudentPortfolio(int studentId) {
		List<Portfolio> portfolio = new ArrayList<>();
		String sqlGetPortfolio = "SELECT * FROM portfolio WHERE studentId = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetPortfolio, studentId);
		
		while(results.next()) {
			portfolio.add(mapRowToPortfolio(results));
		}
		return portfolio;
	}
	
	private List<SocialMedia> getStudentSocialMedia(int studentId) {
		List<SocialMedia> socialMedia = new ArrayList<>();
		String sqlGetSocialMedia = "SELECT name, social_media.socialMediaId, link FROM social_media " +
								   "INNER JOIN student_social_media ssm ON social_media.socialMediaId = ssm.socialMediaId " + 
								   "WHERE studentId = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetSocialMedia, studentId);
		
		while(results.next()) {
			socialMedia.add(mapRowToSocialMedia(results));
		}
		return socialMedia;
	}
	
	private List<SocialMedia> getEmployerSocialMedia(int employerId) {
		List<SocialMedia> socialMedia = new ArrayList<>();
		String sqlGetSocialMedia = "SELECT name, social_media.socialMediaId, link FROM social_media " +
				   				   "INNER JOIN employer_social_media esm ON social_media.socialMediaId = esm.socialMediaId " + 
				   				   "WHERE employerId = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetSocialMedia, employerId);
		
		while(results.next()) {
			socialMedia.add(mapRowToSocialMedia(results));
		}
		return socialMedia;
	}
	
	private Cohort getCohortById(int cohortId) {
		Cohort cohort = new Cohort();
		String sqlGetCohort = "SELECT * FROM cohort WHERE cohortId = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetCohort, cohortId);
		
		if(results.next()) {
			cohort = mapRowToCohort(results);
		}
		return cohort;
	}
	
	private Contact getContactByEmail(String email) {
		Contact contact = new Contact();
		String sqlGetContact = "SELECT * FROM contact WHERE email = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetContact, email);
		
		if(results.next()) {
			contact = mapRowToContact(results);
		}
		return contact;
	}
	
	private Contact getContactById(int contactId) {
		Contact contact = new Contact();
		String sqlGetContact = "SELECT * FROM contact WHERE contactId = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetContact, contactId);
		
		if(results.next()) {
			contact = mapRowToContact(results);
		}
		return contact;
	}
	
	@Override
	public List<String> getAllTechnicalSkills() {
		List<String> techSkills = new ArrayList<String>();
		String sqlGetTechSkills = "SELECT * FROM tech_skills";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetTechSkills);
		while(results.next()) {
			techSkills.add(results.getString("techskillname"));
		}
		return techSkills;
	}
	
	@Override
	public List<String> getAllSoftSkills() {
		List<String> softSkills = new ArrayList<>();
		String sqlGetSoftSkills = "SELECT * FROM soft_skills";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetSoftSkills);
		
		while(results.next()) {
			softSkills.add(results.getString("softSkillName"));
		}
		return softSkills;
	}
	
	@Override
	public List<String> getAllSocialMedia() {
		List<String> socialMedia = new ArrayList<>();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT name FROM social_media");
		
		while(results.next()) {
			socialMedia.add(results.getString("name"));
		}
		return socialMedia;
	}
	
	@Override
	public List<SocialMedia> getSocialMediaNameId() {
		List<SocialMedia> socialMedia = new ArrayList<>();
		SqlRowSet results = jdbcTemplate.queryForRowSet("SELECT * FROM social_media");
		
		while(results.next()) {
			socialMedia.add(mapRowToPartialSocialMedia(results));
		}
		return socialMedia;
	}
	
	@Override
	public String getUserSalt(String userName) {
		String salt = null;
		String sqlGetUserSalt = "SELECT salt FROM appuser WHERE userName = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetUserSalt, userName);
		
		if(results.next()) {
			salt = results.getString("salt").substring(0, 20);
		}
		return salt;
	}
	
	@Override
	public String getUserHash(String userName) {
		String hash = null;
		String sqlGetUserHash = "SELECT password FROM appuser WHERE userName = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetUserHash, userName);
		
		if(results.next()) {
			hash = results.getString("password").substring(0, 5);
		}
		return hash;
	}
	
	//***********************************************************
	//****************** Mapping Methods ************************
	//***********************************************************
	
	private User mapRowToUser(SqlRowSet results) {
		User user = new User();
		user.setUserName(results.getString("userName"));
		user.setUserId(results.getInt("userId"));
		user.setPermission(results.getString("permission"));
		user.setAccountStatus(results.getBoolean("accountStatus"));
		return user;
	}
	
	private Student mapRowToStudent(SqlRowSet results) {
		Student student = new Student();
		int studentId = results.getInt("studentId");
		int cohortId = results.getInt("cohortId");
		int contactId = results.getInt("contactId");
		List<String> techSkills = getStudentTechSkills(studentId);
		List<String> softSkills = getStudentSoftSkills(studentId);
		List<Experience> experiences = getStudentExperience(studentId);
		List<Education> education = getStudentEducation(studentId);
		List<Portfolio> portfolio = getStudentPortfolio(studentId);
		List<SocialMedia> socialMedia = getStudentSocialMedia(studentId);
		student.setStudentId(studentId);
		student.setUserId(results.getInt("userId"));
		student.setDiscoverable(results.getBoolean("isDiscoverable"));
		student.setInterests(results.getString("interests"));
		student.setFirstName(results.getString("firstName"));
		student.setLastName(results.getString("lastName"));
		student.setSummary(results.getString("summary"));
		student.setEducation(education);
		student.setTechSkills(techSkills);
		student.setSoftSkills(softSkills);
		student.setExperiences(experiences);
		student.setPorfolio(portfolio);
		student.setSocialMedia(socialMedia);
		student.setCohort(getCohortById(cohortId));
		student.setContact(getContactById(contactId));
		return student;
	}
	
	private Employer mapRowToEmployer(SqlRowSet results) {
		Employer employer = new Employer();
		int contactId = results.getInt("contactId");
		employer.setBusinessName(results.getString("businessName"));
		employer.setContact(getContactById(contactId));
		employer.setDescription(results.getString("description"));
		employer.setEmployerId(results.getInt("employerId"));
		employer.setFirstName(results.getString("firstName"));
		employer.setLastName(results.getString("lastName"));
		employer.setNotes(results.getString("notes"));
		employer.setUserId(results.getInt("userId"));
		return employer;
	}
	
	private Staff mapRowToStaff(SqlRowSet results) {
		Staff staff = new Staff();
		Contact contact = getContactById(results.getInt("contactId"));
		staff.setContact(contact);
		staff.setFirstName(results.getString("firstName"));
		staff.setLastName(results.getString("lastName"));
		staff.setStaffId(results.getInt("staffId"));
		staff.setUserId(results.getInt("userId"));
		return staff;
	}
	
	private Experience mapRowToExperience(SqlRowSet results) {
		Experience experience = new Experience();
		experience.setCompany(results.getString("company"));
		experience.setDescription(results.getString("description"));
		experience.setEndMonth(results.getString("endMonth"));
		experience.setEndYear(results.getString("endYear"));
		experience.setExperienceId(results.getInt("experienceId"));
		experience.setStartMonth(results.getString("startMonth"));
		experience.setStartYear(results.getString("startYear"));
		experience.setStudentId(results.getInt("studentId"));
		experience.setTitle(results.getString("title"));
		return experience;
	}
	
	private Education mapRowToEducation(SqlRowSet results) {
		Education education = new Education();
		education.setDegree(results.getString("degree"));
		education.setDescription(results.getString("description"));
		education.setEducationId(results.getInt("educationId"));
		education.setEndMonth(results.getString("endMonth"));
		education.setEndYear(results.getString("endYear"));
		education.setFieldOfStudy(results.getString("fieldOfStudy"));
		education.setSchool(results.getString("school"));
		education.setStartMonth(results.getString("startMonth"));
		education.setStartYear(results.getString("startYear"));
		education.setStudentId(results.getInt("studentId"));
		return education;
	}
	
	private Cohort mapRowToCohort(SqlRowSet results) {
		Cohort cohort = new Cohort();
		cohort.setCohortId(results.getInt("cohortId"));
		cohort.setCohortNumber(results.getInt("cohortNumber"));
		cohort.setCurrent(results.getBoolean("isCurrent"));
		cohort.setLocation(results.getString("location"));
		cohort.setProgrammingLang(results.getString("programmingLang"));
		return cohort;
	}
	
	private Contact mapRowToContact(SqlRowSet results) {
		Contact contact = new Contact();
		contact.setAddress1(results.getString("address1"));
		contact.setAddress2(results.getString("address2"));
		contact.setCity(results.getString("city"));
		contact.setContactId(results.getInt("contactId"));
		contact.setContactPref(results.getString("contactPref"));
		contact.setEmail(results.getString("email"));
		contact.setPhone(results.getString("phone"));
		contact.setState(results.getString("state"));
		contact.setZip(results.getString("zip"));
		return contact;
	}
	
	private Portfolio mapRowToPortfolio(SqlRowSet results) {
		Portfolio portfolio = new Portfolio();
		portfolio.setDescription(results.getString("description"));
		portfolio.setLink(results.getString("link"));
		portfolio.setPortfolioId(results.getInt("portfolioId"));
		portfolio.setProjectName(results.getString("projectName"));
		portfolio.setStudentId(results.getInt("studentId"));
		return portfolio;
	}
	
	private SocialMedia mapRowToSocialMedia(SqlRowSet results) {
		SocialMedia socialMedia = new SocialMedia();
		socialMedia.setLink(results.getString("link"));
		socialMedia.setName(results.getString("name"));
		socialMedia.setSocialMediaId(results.getInt("socialMediaId"));
		return socialMedia;
	}
	
	private SocialMedia mapRowToPartialSocialMedia(SqlRowSet results) {
		SocialMedia socialMedia = new SocialMedia();
		socialMedia.setName(results.getString("name"));
		socialMedia.setSocialMediaId(results.getInt("socialMediaId"));
		return socialMedia;
	}

	@Override
	public boolean searchForUsernameAndPassword(String userName, String password) {
		String sqlSearchForUser = "SELECT * "+
			      "FROM appuser " +
			      "WHERE userName = ?";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchForUser, userName);
		
		if(results.next()) {
			String storedSalt = results.getString("salt");
			String storedPassword = results.getString("password");
			boolean accountStatus = results.getBoolean("accountStatus");
			String hashedPassword = passwordHasher.computeHash(password, Base64.decode(storedSalt));
			if(storedPassword.equals(hashedPassword) && accountStatus) {
				return true;
			}
			else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	//***********************************************************
	//***************** Update / Add Methods ********************
	//***********************************************************
	
	@Override
	public void addNewStaff(NewUser newUser) {
		addNewUser(newUser);
		int contactId = addNewContact(newUser.getEmail());
		int userId = getUserByUserName(newUser.getUserName()).getUserId();
		jdbcTemplate.update("INSERT INTO staff (userId, firstName, lastName, contactId) VALUES (?,?,?,?)",
											   userId, newUser.getFirstName(), newUser.getLastName(), contactId);
	}
	
	@Override
	public void addNewStudent(NewUser newUser) {
		addNewUser(newUser);
		int contactId = addNewContact(newUser.getEmail());
		int userId = getUserByUserName(newUser.getUserName()).getUserId();
		jdbcTemplate.update("INSERT INTO student (userId, firstName, lastName, cohortId, contactId) VALUES (?,?,?,?,?)",
												 userId, newUser.getFirstName(), newUser.getLastName(), newUser.getCohortId(), contactId);
	}
	
	@Override
	public void addNewEmployer(NewUser newUser) {
		addNewUser(newUser);
		int contactId = addNewContact(newUser.getEmail());
		int userId = getUserByUserName(newUser.getUserName()).getUserId();
		jdbcTemplate.update("INSERT INTO employer (userId, contactId, businessName, firstName, lastName) VALUES (?,?,?,?,?)",
				   							   userId, contactId, newUser.getBusinessName(), newUser.getFirstName(), newUser.getLastName());
	}
	
	@Override
	public void addNewEducation(Education education) {
		jdbcTemplate.update("INSERT INTO education (studentId, school, startYear, startMonth, endMonth, endYear, degree, fieldOfStudy, description)" +
							"VALUES (?,?,?,?,?,?,?,?,?)", education.getStudentId(), education.getSchool(), education.getStartYear(), 
							education.getStartMonth(), education.getEndMonth(), education.getEndYear(), education.getDegree(), 
							education.getFieldOfStudy(), education.getDescription());
	}
	
	@Override
	public void addNewEexperience(Experience experience) {
		jdbcTemplate.update("INSERT INTO experience (studentId, company, startYear, startMonth, endMonth, endYear, title, description)" +
							"VALUES (?,?,?,?,?,?,?,?)", experience.getStudentId(), experience.getCompany(), experience.getStartYear(), 
							experience.getStartMonth(), experience.getEndMonth(), experience.getEndYear(),
							experience.getTitle(), experience.getDescription());
	}
	
	@Override
	public void addNewTechSkill(String techSkill) {
		jdbcTemplate.update("INSERT INTO tech_skills (techSkillName) VALUES (?)", techSkill);
	}
	
	@Override
	public void addNewSocialMedia(int studentId, SocialMedia socialMedia) {
		jdbcTemplate.update("INSERT INTO student_social_media (studentId, socialMediaId, link) VALUES (?,?,?)",
							studentId, socialMedia.getSocialMediaId(), socialMedia.getLink());
	}
	
	@Override
	public void addNewSocialMedia(int studentId, String name, String link) {
		jdbcTemplate.update("INSERT INTO student_social_media (studentId, link, socialMediaId) " +
							"VALUES (?,?,SELECT socialMediaId FROM social_media WHERE name = ?)", studentId, link, name);
	}
	
	@Override
	public void addStudentTechSkill(int studentId, String techSkill) {
		jdbcTemplate.update("INSERT INTO student_tech_skills (studentId, techSkillId) VALUES (?,(SELECT techSkillId FROM tech_skills WHERE techSkillName = ?))",
							studentId, techSkill);
	}
	
	@Override 
	public void addStudentTechSkills(int studentId, List<String> techSkills) {
		for(String techSkill : techSkills) {
			addStudentTechSkill(studentId, techSkill);
		}
	}
	
	@Override
	public void addStudentSoftSkill(int studentId, String softSkill) {
		jdbcTemplate.update("INSERT INTO student_soft_skills (studentId, softSkillId) VALUES (?,(SELECT softSkillId FROM soft_skills WHERE softSkillName = ?))",
							studentId, softSkill);
	}
	
	@Override
	public void addStudentSoftSkills(int studentId, List<String> softSkills) {
		for(String softSkill : softSkills) {
			addStudentSoftSkill(studentId, softSkill);
		}
	}
	
	@Override
	public void addNewPortfolio(Portfolio portfolio) {
		jdbcTemplate.update("INSERT INTO portfolio (studentId, projectName, description, link) VALUES (?,?,?,?)",
							portfolio.getStudentId(), portfolio.getProjectName(), portfolio.getDescription(), portfolio.getLink());
	}
	
	@Override
	public void addNewCohort(Cohort cohort) {
		jdbcTemplate.update("INSERT INTO cohort (location, programmingLang, cohortNumber) VALUES (?,?,?)",
							cohort.getLocation(), cohort.getProgrammingLang(), cohort.getCohortNumber());
	}
	
	//************************Update Methods****************************
	
	@Override
	public void updatePassword(String userName, String password) {
		byte[] salt = passwordHasher.generateRandomSalt();
		String hashedPassword = passwordHasher.computeHash(password, salt);
		String saltString = new String(Base64.encode(salt));
		jdbcTemplate.update("UPDATE appuser SET password = ?, salt = ? WHERE userName = ?", hashedPassword, saltString, userName);
	}
	
	@Override
	public void updateStudent(Student student) {
		jdbcTemplate.update("UPDATE student SET firstName=?, lastName=?, isDiscoverable=?, summary=?, interests=? WHERE studentId=?",
							student.getFirstName(), student.getLastName(), student.isDiscoverable(), student.getSummary(),
							student.getInterests(), student.getStudentId());
		updateContact(student.getContact());
	}
	
	@Override
	public void updateEmployer(Employer employer) {
		jdbcTemplate.update("UPDATE employer SET businessName=?, firstName=?, lastName=?, description=? WHERE userId=?",
							employer.getBusinessName(), employer.getFirstName(), employer.getLastName(), employer.getDescription(), employer.getUserId());
	}
	
	@Override
	public void updateStaff(Staff staff) {
		jdbcTemplate.update("UPDATE staff SET firstName=?, lastName=? WHERE userId=?",
							staff.getFirstName(), staff.getLastName(), staff.getUserId());
	}
	
	@Override
	public void updateEducation(Education education) {
		jdbcTemplate.update("UPDATE education SET school=?, startYear=?, startMonth=?, endMonth=?, endYear=?, degree=?, fieldOfStudy=?, description=? WHERE educationId = ?",
							education.getSchool(), education.getStartYear(), education.getStartMonth(), education.getEndMonth(), education.getEndYear(), 
							education.getDegree(), education.getFieldOfStudy(), education.getDescription(), education.getEducationId());
	}
	
	@Override
	public void updateExperience(Experience experience) {
		jdbcTemplate.update("UPDATE experience SET company=?, startYear=?, startMonth=?, endMonth=?, endYear=?, title=?, description=? WHERE experienceId = ?",
							experience.getCompany(), experience.getStartYear(), experience.getStartMonth(), experience.getEndMonth(), experience.getEndYear(), 
							experience.getTitle(), experience.getDescription(), experience.getExperienceId());
	}
	
	@Override
	public void updatePortfolio(Portfolio portfolio) {
		jdbcTemplate.update("UPDATE portfolio SET projectName=?, description=?, link=? WHERE portfolioId = ?",
							portfolio.getProjectName(), portfolio.getDescription(), portfolio.getLink(), portfolio.getPortfolioId());
	}
	
	@Override
	public void updateSetUserActive(User user) {
		jdbcTemplate.update("UPDATE appuser SET accountStatus=true WHERE userId = ?", user.getUserId());
	}
	
	@Override
	public void updateSetUserActive(String salt) {
		salt += "%";
		jdbcTemplate.update("UPDATE appuser SET accountStatus = ? WHERE salt LIKE (?)", true, salt);
	}
	
	@Override
	public void updateSetUserInactive(User user) {
		jdbcTemplate.update("UPDATE appuser SET accountStatus=false WHERE userId = ?", user.getUserId());
	}
	
	@Override
	public void updateSocialMedia(SocialMedia socialMedia, int studentId) {
		jdbcTemplate.update("UPDATE student_social_media SET link=? WHERE studentId=? AND socialMediaId=?",
							socialMedia.getLink(), studentId, socialMedia.getSocialMediaId());
	}
	
	//************************Delete Methods****************************
	
	@Override
	public void deleteEmployer(Employer employer) {
		jdbcTemplate.update("DELETE FROM employer WHERE employerId = ?", employer.getEmployerId());
		jdbcTemplate.update("DELETE FROM contact WHERE contactId = ?", employer.getContact().getContactId());
		jdbcTemplate.update("DELETE FROM appuser WHERE userId = ?", employer.getUserId());
	}
	
	@Override
	public void deleteStudent(Student student) {
		int studentId = student.getStudentId();
		deleteStudentSocialMedias(studentId, student.getSocialMedia());
		deleteStudentSoftSkills(studentId, student.getSoftSkills());
		deleteStudentTechSkills(studentId, student.getTechSkills());
		deleteEducations(student.getEducation());
		deleteExperience(student.getExperiences());
		deletePortfolio(student.getPortfolio());
		jdbcTemplate.update("DELETE FROM student WHERE studentId = ?", studentId);
		jdbcTemplate.update("DELETE FROM contact WHERE contactId = ?", student.getContact().getContactId());
		jdbcTemplate.update("DELETE FROM appuser WHERE userId = ?", student.getUserId());
	}
	
	@Override
	public void deleteStaff(Staff staff) {
		jdbcTemplate.update("DELETE FROM staff WHERE staffId = ?", staff.getStaffId());
		jdbcTemplate.update("DELETE FROM contact WHERE contactId = ?", staff.getContact().getContactId());
		jdbcTemplate.update("DELETE FROM appuser WHERE userId = ?", staff.getUserId());
	}
	
	@Override
	public void deleteEducation(Education education) {
		jdbcTemplate.update("DELETE FROM education WHERE educationId = ?", education.getEducationId());
	}
	
	@Override
	public void deleteEducations(List<Education> educations) {
		for(Education education : educations) {
			deleteEducation(education);
		}
	}
	
	@Override
	public void deleteExperience(Experience experience) {
		jdbcTemplate.update("DELETE FROM experience WHERE experienceId = ?", experience.getExperienceId());
	}
	
	@Override
	public void deleteExperience(List<Experience> experiences) {
		for(Experience experience : experiences) {
			deleteExperience(experience);
		}
	}
	
	@Override
	public void deletePortfolio(Portfolio portfolio) {
		jdbcTemplate.update("DELETE FROM portfolio WHERE portfolioId = ?", portfolio.getPortfolioId());
	}
	
	@Override
	public void deletePortfolio(List<Portfolio> projects) {
		for(Portfolio project : projects) {
			deletePortfolio(project);
		}
	}
	
	@Override
	public void deleteTechSkill(String techSkill) {
		jdbcTemplate.update("DELETE FROM tech_skills WHERE techSkillName = ?", techSkill);
	}
	
	@Override
	public void deleteStudentTechSkill(int studentId, String techSkill) {
		jdbcTemplate.update("DELETE FROM student_tech_skills WHERE studentId = ? AND techSkillID = (SELECT techSkillId FROM tech_skills WHERE techSkillName = ?)",
							studentId, techSkill);
	}
	
	@Override
	public void deleteStudentTechSkills(int studentId, List<String> techSkills) {
		for(String techSkill : techSkills) {
			deleteStudentTechSkill(studentId, techSkill);
		}
	}
	
	@Override
	public void deleteStudentSoftSkill(int studentId, String softSkill) {
		jdbcTemplate.update("DELETE FROM student_soft_skills WHERE studentId = ? AND softSkillID = (SELECT softSkillId FROM soft_skills WHERE softSkillName = ?)",
							studentId, softSkill);
	}
	
	@Override
	public void deleteStudentSoftSkills(int studentId, List<String> softSkills) {
		for(String softSkill : softSkills) {
			deleteStudentSoftSkill(studentId, softSkill);
		}
	}
	
	@Override
	public void deleteStudentSocialMedia(int studentId, SocialMedia socialMedia) {
		jdbcTemplate.update("DELETE FROM student_social_media WHERE studentId = ? AND socialMediaId = ?", studentId, socialMedia.getSocialMediaId());
	}
	
	@Override 
	public void deleteStudentSocialMedias(int studentId, List<SocialMedia> socialMedias) {
		for(SocialMedia socialMedia : socialMedias) {
			deleteStudentSocialMedia(studentId, socialMedia);
		}
	}
	
	@Override
	public void deleteCohort(Cohort cohort) {
		jdbcTemplate.update("DELETE FROM cohort WHERE cohortId = ?", cohort.getCohortId());
	}
	
	//************************Private Methods****************************
	
	private void updateContact(Contact contact) {
		jdbcTemplate.update("UPDATE contact SET contactPref=?, email=?, phone=?, address1=?, address2=?, city=?, state=?, zip=? WHERE contactId=?",
							contact.getContactPref(), contact.getEmail(), contact.getPhone(), contact.getAddress1(), contact.getAddress2(), 
							contact.getCity(), contact.getState(), contact.getZip(), contact.getContactId());
	}
	
	private int addNewContact(String email) {
		jdbcTemplate.update("INSERT INTO contact (email) VALUES (?)", email);
		int contactId = getContactByEmail(email).getContactId();
		return contactId;
	}
	
	private void addNewUser(NewUser newUser) {
		byte[] salt = passwordHasher.generateRandomSalt();
		String hashedPassword = passwordHasher.computeHash(newUser.getPassword(), salt);
		String saltString = new String(Base64.encode(salt));
		jdbcTemplate.update("INSERT INTO appuser (userName, password, permission, salt) VALUES (?,?,?,?)",
												 newUser.getUserName(), hashedPassword, newUser.getPermission(), saltString);
	}
}
