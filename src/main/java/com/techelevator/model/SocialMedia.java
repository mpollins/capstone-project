package com.techelevator.model;

public class SocialMedia {
	private int socialMediaId;
	private String name;
	private String link;
	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public int getSocialMediaId() {
		return socialMediaId;
	}
	public void setSocialMediaId(int socialMediaId) {
		this.socialMediaId = socialMediaId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}

