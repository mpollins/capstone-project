package com.techelevator.model;

import java.util.List;

public class Employer {
	private int employerId;
	private int userId;
	private Contact contact;
	private String businessName;
	private String firstName;
	private String lastName;
	private String description;
	private String notes;
	//private List<SocialMedia> socialMedia;
	
//	public List<SocialMedia> getSocialMedia() {
//		return socialMedia;
//	}
//	public void setSocialMedia(List<SocialMedia> socialMedia) {
//		this.socialMedia = socialMedia;
//	}
	
	public int getEmployerId() {
		return employerId;
	}
	public void setEmployerId(int employerId) {
		this.employerId = employerId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
}
