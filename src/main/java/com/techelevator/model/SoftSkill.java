package com.techelevator.model;

public class SoftSkill {
	private int softSkillId;
	private String softSkillName;
	
	public int getSoftSkillId() {
		return softSkillId;
	}
	public void setSoftSkillId(int softSkillId) {
		this.softSkillId = softSkillId;
	}
	public String getSoftSkillName() {
		return softSkillName;
	}
	public void setSoftSkillName(String softSkillName) {
		this.softSkillName = softSkillName;
	}
}
