package com.techelevator.controller;

import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.model.Cohort;
import com.techelevator.model.Contact;
import com.techelevator.model.Education;
import com.techelevator.model.Employer;
import com.techelevator.model.Experience;
import com.techelevator.model.NewUser;
import com.techelevator.model.Portfolio;
import com.techelevator.model.SocialMedia;
import com.techelevator.model.Staff;
import com.techelevator.model.Student;
import com.techelevator.model.User;
import com.techelevator.model.UserDAO;
import com.techelevator.security.PasswordHasher;

@Controller
@SessionAttributes({"currentUser", "user"})
@Transactional
public class UserController {

	private UserDAO userDAO;
	List <String> months;

	@Autowired
	public UserController(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@RequestMapping(path="/users/{userName}/newUser", method=RequestMethod.GET)
	public String displayNewUserForm(ModelMap model, @PathVariable String userName) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
			model.put("cohortList", userDAO.getCohorts());
			PasswordHasher hasher = new PasswordHasher();
			byte[] salt = hasher.generateRandomSalt();
			String newUserPassword = new String(Base64.encode(salt)).substring(0, 10);
			model.put("password", newUserPassword);
			return "newUser";
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/newUser", method=RequestMethod.POST)
	public String createUser(ModelMap model, NewUser newUser, @RequestParam int permission, @PathVariable String userName) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
			newUser.setPermission(permission);
			if(newUser.getPermission() == 0) {
				userDAO.addNewStaff(newUser);
			}
			else if (newUser.getPermission() == 1) {
				userDAO.addNewStudent(newUser);
			}
			else if(newUser.getPermission() == 2) {
				userDAO.addNewEmployer(newUser);
			}
			String confirmationSalt = userDAO.getUserSalt(newUser.getUserName());
			String confirmationLink = "http://localhost:8080/capstone/newUserAccountConfirm" + confirmationSalt;
			model.put("password", newUser.getPassword());
			model.put("userName", userName);
			model.put("user", userDAO.getUserByUserName(userName));
			model.put("newUser", newUser);
			model.put("confirmationLink", confirmationLink);
			return "sendUserActivation";
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/newCohort", method=RequestMethod.GET)
	public String createCohortForm(ModelMap model, @PathVariable String userName) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
			model.put("allTechSkills", userDAO.getAllTechnicalSkills());
			model.put("cohorts", userDAO.getCohorts());
			return "newCohort";			
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/newCohort", method=RequestMethod.POST)
	public String createCohort(Cohort cohort) {
		userDAO.addNewCohort(cohort);
		return "redirect:/users/{userName}/viewAllCohorts";
	}
	
	@RequestMapping(path="/users/{userName}/newTechSkill", method=RequestMethod.GET)
	public String createTechSkillForm(ModelMap model, @PathVariable String userName) {
		model.put("techSkills", userDAO.getAllTechnicalSkills());
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
			return "newTechSkill";
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/newTechSkill", method=RequestMethod.POST)
	public String createTechSkill(String techSkill) {
		userDAO.addNewTechSkill(techSkill);;
		return "redirect:/users/{userName}/newTechSkill";
	}
	
	@RequestMapping(path="/users/{userName}/deleteTechSkill", method=RequestMethod.POST)
	public String deleteTechSkill(String techSkill) {
		userDAO.deleteTechSkill(techSkill);;
		return "redirect:/users/{userName}/newTechSkill";
	}
	
	@RequestMapping(path="/users/{userName}", method=RequestMethod.GET)
	public String displayDashboard(ModelMap model, @PathVariable String userName) {
		User user = userDAO.getUserByUserName(userName);
		if(user.getPermission().equals("0")) {
			model.put("currentUser", userName);
			model.put("staff", userDAO.getStaffByUserId(user.getUserId()));
			model.put("user", user);
			return "staffDashboard";
		}
		else if(user.getPermission().equals("1")) {
			model.put("currentUser", userName);
			model.put("student", userDAO.getStudentByUserId(user.getUserId()));
			model.put("user", user);
			return "studentDashboard";
		}
		else if (user.getPermission().equals("2")) {
			model.put("currentUser", userName);
			model.put("employer", userDAO.getEmployerByUserId(user.getUserId()));
			model.put("user", user);
			return "employerDashboard";
		}
		else {
			return "redirect:/login";
		}
	}
	
	@RequestMapping(path="/users/{userName}/viewAllUsers", method=RequestMethod.GET)
	public String viewAllUsers(ModelMap model, @PathVariable String userName) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
			model.put("allUsers", userDAO.getAllUsers());
			return "allUsers";
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/viewAllCohorts", method=RequestMethod.GET)
	public String viewAllCohorts(ModelMap model, @PathVariable String userName) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
			model.put("allCohorts", userDAO.getCohorts());
			return "allCohorts";
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/deleteCohort", method=RequestMethod.POST)
	public String deleteCohort(ModelMap model, @PathVariable String userName, int cohortId, boolean delete) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
			List<Cohort> cohorts = userDAO.getCohorts();
			for(Cohort cohort : cohorts) {
				if(cohort.getCohortId() == cohortId) {
					userDAO.deleteCohort(cohort);
				}
			}
				
				return "redirect:/users/{userName}/viewAllEmployers";
		}
		else {
			return "redirect:/users/{userName}/viewAllEmployers";
		}
	}
	
	@RequestMapping(path="/users/{userName}/viewAllStudents", method=RequestMethod.GET)
	public String viewAllStudents(ModelMap model, @PathVariable String userName) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
			model.put("allStudents", userDAO.getPublicStudents());
			model.put("cohorts", userDAO.getCohorts());
			return "allStudents";
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/deleteStudent", method=RequestMethod.POST)
	public String deleteIndividualStudentAccount(ModelMap model, @PathVariable String userName, int studentId, boolean delete) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
				Student student = userDAO.getStudentById(studentId);
				userDAO.deleteStudent(student);
				return "redirect:/users/{userName}/viewAllStudents";
		}
		else {
			return "redirect:/users/{userName}/viewAllStudents";
		}
	}
	
	@RequestMapping(path="/users/{userName}/viewAllStaff", method=RequestMethod.GET)
	public String viewAllStaff(ModelMap model, @PathVariable String userName) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
			model.put("allStaff", userDAO.getAllStaff());
			return "allStaff";
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/deleteStaff", method=RequestMethod.POST)
	public String deleteStaff(ModelMap model, @PathVariable String userName, boolean delete, int userId, int staffId) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
//				userDAO.deleteUser(userDAO.getUserByUserId(userId));
//				userDAO.deleteStaff(userDAO.getStaffByUserId(userId));
				return "redirect:/users/{userName}/viewAllStaff";
		}
		else {
			return "redirect:/users/{userName}/viewAllAllStaff";
		}
	}
	
	@RequestMapping(path="/users/{userName}/viewAllEmployers", method=RequestMethod.GET)
	public String viewAllEmployers(ModelMap model, @PathVariable String userName) {
		User user = userDAO.getUserByUserName(userName);
		if(user.getPermission().equals("0") || user.getPermission().equals("1")) {
			model.put("thisUser", user);
			model.put("allEmployers", userDAO.getAllEmployers());
			return "allEmployers";
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/deleteEmployer", method=RequestMethod.POST)
	public String deleteIndividualEmployerAccount(ModelMap model, @PathVariable String userName, int userId, int employerId, boolean delete) {
		if(userDAO.getUserByUserName(userName).getPermission().equals("0")) {
				userDAO.deleteEmployer(userDAO.getEmployerByUserId(userId));
				return "redirect:/users/{userName}/viewAllEmployers";
		}
		else {
			return "redirect:/users/{userName}/viewAllEmployers";
		}
	}
	
	@RequestMapping(path="/users/{userName}/userProfile", method=RequestMethod.GET)
	public String viewUserProfileGet(ModelMap model, @RequestParam int userId, @PathVariable String userName) {
		User user = userDAO.getUserByUserId(userId);
		if(user.getPermission().equals("0")) {
			model.put("staff", userDAO.getStaffByUserId(userId));
			return "staffProfileDetailed";
		}
		else if(user.getPermission().equals("1")) {
			model.put("student", userDAO.getStudentByUserId(userId));
			return "studentProfileDetailed";
		}
		else {
			model.put("employer", userDAO.getEmployerByUserId(userId));
			return "employerProfileDetailed";
		}
	}
	
	@RequestMapping(path="/users/{userName}/studentProfile", method=RequestMethod.GET)
	public String viewStudentProfile(ModelMap model, @PathVariable String userName, @RequestParam int studentId) {
		Student student = userDAO.getStudentById(studentId);
		model.put("student", student);
		return "studentProfileDetailed";
	}
	
	@RequestMapping(path="/users/{userName}/employerProfile", method=RequestMethod.GET)
	public String viewEmployerProfile(ModelMap model, @PathVariable String userName, @RequestParam int userId) {
		Employer employer = userDAO.getEmployerByUserId(userId);
		model.put("employer", employer);
		return "employerProfileDetailed";
	}
	
	@RequestMapping(path="/users/{userName}/staffProfile", method=RequestMethod.GET)
	public String viewStaffProfile(ModelMap model, @PathVariable String userName, @RequestParam int userId) {
		Staff staff = userDAO.getStaffByUserId(userId);
		model.put("staff", staff);
		return "staffProfileDetailed";
	}
	
	@RequestMapping(path="users/{userName}/cohortUserView{city}", method=RequestMethod.GET)
	public String viewCohortStudentsUser(ModelMap model, @PathVariable String city) {
		model.put("students", userDAO.getSearchableStudents(city));
		model.put("cohorts", userDAO.getCohorts());
		model.put("city", city);
		return "cohortUserView";
	}
	
	@RequestMapping(path="/users/{userName}/editProfile", method=RequestMethod.GET)
	public String updateStudentProfilePage(ModelMap model, @ModelAttribute("user") User user, @PathVariable String userName) {
		if(user.getPermission().equals("0")) {
			model.put("staff", userDAO.getStaffByUserId(user.getUserId()));
			return "editStaffProfile";
		}
		else if(user.getPermission().equals("1")) {
			model.put("student", userDAO.getStudentByUserId(user.getUserId()));
			return "editStudentProfile";
		}
		else {
			model.put("employer", userDAO.getEmployerByUserId(user.getUserId()));
			return "editEmployerProfile";
		}
		
	}
	
	@RequestMapping(path="/users/{userName}/editProfile", method=RequestMethod.POST)
	public String updateStudentProfile(ModelMap model, Student student, Contact contact, @ModelAttribute("user") User user, @PathVariable String userName, Staff staff, Employer employer) {
		if(user.getPermission().equals("0")) {
			userDAO.updateStaff(staff);
			model.put("staff", userDAO.getStaffByUserId(user.getUserId()));
			model.put("userName", userName);
			return "redirect:/users/{userName}/editProfile";
		}
		else if(user.getPermission().equals("1")) {
			student.setContact(contact);
			userDAO.updateStudent(student);
			model.put("student", userDAO.getStudentByUserId(user.getUserId()));
			model.put("userName", userName);
			return "studentProfileDetailed";
		}
		else if(user.getPermission().equals("2")) {
			userDAO.updateEmployer(employer);
			model.put("employer", userDAO.getEmployerByUserId(user.getUserId()));
			model.put("userName", userName);
			return "redirect:/users/{userName}/editProfile";
		}
		else {
			return "redirect:/users/{userName}";
		}
	}
	
	@RequestMapping(path="/users/{userName}/editEducation", method=RequestMethod.GET)
	public String editEducationInfo(ModelMap model, @PathVariable String userName) {
		User user = userDAO.getUserByUserName(userName);
		months = new ArrayList<String>();
		months.add("January");
		months.add("February");
		months.add("March");
		months.add("April");
		months.add("May");
		months.add("June");
		months.add("July");
		months.add("August");
		months.add("September");
		months.add("October");
		months.add("November");
		months.add("December");
		model.put("months", months);
		model.put("student", userDAO.getStudentByUserId(user.getUserId()));
		model.put("userName", userName);
		return "editStudentEducation";
	}
	
	@RequestMapping(path="/users/{userName}/editEducation", method=RequestMethod.POST)
	public String editEducationInfoPost(ModelMap model, @PathVariable String userName, Education education, boolean delete) {
		if (education.getEducationId() == 0) {
			userDAO.addNewEducation(education);
		}
		else {
			if(delete) {
				userDAO.deleteEducation(education);
			}
			else {
				userDAO.updateEducation(education);
			}
		}
		return "redirect:/users/{userName}/editEducation";
	}
	
	@RequestMapping(path="/users/{userName}/editExperience", method=RequestMethod.GET)
	public String editExperienceInfoGet(ModelMap model, @PathVariable String userName) {
		User user = userDAO.getUserByUserName(userName);
		months = new ArrayList<String>();
		months.add("January");
		months.add("February");
		months.add("March");
		months.add("April");
		months.add("May");
		months.add("June");
		months.add("July");
		months.add("August");
		months.add("September");
		months.add("October");
		months.add("November");
		months.add("December");
		model.put("months", months);
		model.put("student", userDAO.getStudentByUserId(user.getUserId()));
		model.put("userName", userName);
		return "editStudentExperience";
	}
	
	@RequestMapping(path="/users/{userName}/editExperience", method=RequestMethod.POST)
	public String editExperienceInfoPost(ModelMap model, @PathVariable String userName, Experience experience, boolean delete) {
		if (experience.getExperienceId() == 0) {
			userDAO.addNewEexperience(experience);
		}
		else {
			if(delete) {
				userDAO.deleteExperience(experience);
			}
			else {
				userDAO.updateExperience(experience);
			}
			
		}
		return "redirect:/users/{userName}/editExperience";
	}
	
	@RequestMapping(path="/users/{userName}/editPortfolio", method=RequestMethod.GET)
	public String editPortfolioGet(ModelMap model, @PathVariable String userName) {
		model.put("userName", userName);
		model.put("student", userDAO.getStudentByUserId(userDAO.getUserByUserName(userName).getUserId()));
		return "editStudentPortfolio";
	}
	
	@RequestMapping(path="/users/{userName}/editPortfolio", method=RequestMethod.POST)
	public String editPortfolioPost(ModelMap model, @PathVariable String userName, Portfolio portfolio, boolean delete) {
		if (portfolio.getPortfolioId() == 0) {
			userDAO.addNewPortfolio(portfolio);
		}
		else {
			if(delete) {
				userDAO.deletePortfolio(portfolio);
			}
			else {
				userDAO.updatePortfolio(portfolio);
			}
		}
		return "redirect:/users/{userName}/editPortfolio";
	}
	
	@RequestMapping(path="/users/{userName}/editSkills", method=RequestMethod.GET)
	public String editSkillsGet(ModelMap model, @PathVariable String userName) {
		User user = userDAO.getUserByUserName(userName);
		
		model.put("studentTech", userDAO.getStudentByUserId(user.getUserId()).getTechSkills());
		model.put("studentSoft", userDAO.getStudentByUserId(user.getUserId()).getSoftSkills());
		model.put("techSkillsList", userDAO.getAllTechnicalSkills());
		model.put("softSkillsList", userDAO.getAllSoftSkills());
		return "editStudentSkills";
	}
	
	@RequestMapping(path="/users/{userName}/editSkills", method=RequestMethod.POST)
	public String editSkillsPost(ModelMap model, @RequestParam("allTechSkills") String allTechSkills, @RequestParam("allSoftSkills") String allSoftSkills, @PathVariable String userName) {
		User user = userDAO.getUserByUserName(userName);
		Student student = userDAO.getStudentByUserId(user.getUserId());
		List<String> currentUserTechSkills = student.getTechSkills();
		List<String> currentUserSoftSkills = student.getSoftSkills();
		List<String> techAddList = new ArrayList<>();
		List<String> softAddList = new ArrayList<>();
		
		for(String addTech : allTechSkills.split(",")) {
			if(currentUserTechSkills.contains(addTech)) {
				currentUserTechSkills.remove(addTech);
			} else {
				techAddList.add(addTech);
			}
		}
		
		for(String addSoft : allSoftSkills.split(",")) {
			if(currentUserSoftSkills.contains(addSoft)) {
				currentUserSoftSkills.remove(addSoft);
			} else {
				softAddList.add(addSoft);
			}
		}

		userDAO.addStudentTechSkills(student.getStudentId(), techAddList);
		userDAO.deleteStudentTechSkills(student.getStudentId(), currentUserTechSkills);
		userDAO.addStudentSoftSkills(student.getStudentId(), softAddList);
		userDAO.deleteStudentSoftSkills(student.getStudentId(), currentUserSoftSkills);
		
		return "redirect:/users/{userName}/editSkills";
	}
	
	@RequestMapping(path="/users/{userName}/editSocialMedia", method=RequestMethod.GET)
	public String editSocialMediaGet(ModelMap model, @PathVariable(value="userName") String userName) {
		User thisUser = userDAO.getUserByUserName(userName);
		Student thisStudent;
		if(thisUser.getPermission().equals("1")) {
			thisStudent = userDAO.getStudentByUserId(thisUser.getUserId());
			List<String> notPresent = new ArrayList<String>();
			List<String> allTech = userDAO.getAllSocialMedia();
			for(int sm = 0; sm < allTech.size(); sm++) {
				if(thisStudent.getSocialMedia().size() > sm ){
					if(thisStudent.getSocialMedia().get(sm).getName().equals(allTech.get(sm))) {
					}
					else {
						notPresent.add(allTech.get(sm));
					}
				}
				else {
					notPresent.add(allTech.get(sm));
				}
			}
			model.put("student", thisStudent);
			model.put("notPresent", notPresent);
			model.put("allSocialMedia", userDAO.getAllSocialMedia());
			model.put("userName", userName);
			model.put("user", thisUser);
			return "editSocialMedia";
		}
		else if(thisUser.getPermission().equals("2")) {
			Employer thisEmployer = userDAO.getEmployerByUserId(thisUser.getUserId());
//			model.put("socialMedia", thisEmployer.getSocialMedia());
			model.put("userName", userName);
			model.put("user", thisUser);
			return "editSocialMedia";
		}
		else {
			return "redirect:/";
		}
	}
	
	@RequestMapping(path="/users/{userName}/editSocialMedia", method=RequestMethod.POST)
	public String editSocialMediaPost(ModelMap model, @PathVariable(value="userName") String userName, SocialMedia socialMedia, @RequestParam int userId, boolean delete) {
		User thisUser = userDAO.getUserByUserName(userName);
		if(thisUser.getPermission().equals("1")) {
			if(socialMedia.getSocialMediaId() == 0) {
				for(SocialMedia sm : userDAO.getSocialMediaNameId()) {
					if(sm.getName().equals(socialMedia.getName())) {
						socialMedia.setSocialMediaId(sm.getSocialMediaId());
						userDAO.addNewSocialMedia(userDAO.getStudentByUserId(userId).getStudentId(), socialMedia);
					}
				}
			}
			else {
				if(delete) {
					userDAO.deleteStudentSocialMedia(userDAO.getStudentByUserId(userId).getStudentId(), socialMedia);
				}
				else {
					userDAO.updateSocialMedia(socialMedia, userDAO.getStudentByUserId(userId).getStudentId());
				}
				
			}
			return "redirect:/users/{userName}/editSocialMedia";
		}
		else if(thisUser.getPermission().equals("2")) {
			if(socialMedia.getSocialMediaId() == 0) {
//				userDAO.addNewSocialMedia(userDAO.getEmployerByUserId().getEmployerId(), socialMedia);
			}
			else {
//				userDAO.updateSocialMedia(userDAO.getEmployerByUserId().getEmployerId(), socialMedia);
			}
			return "redirect:/users/{userName}/editSocialMedia";
		}
		else {
			return "redirect:/";
		}
	}
	
	@RequestMapping(path="/users/{userName}/changePassword", method=RequestMethod.GET)
	public String displayChangePasswordForm(ModelMap model, @PathVariable String userName) {
		model.put("userName", userName);
		model.put("user", userDAO.getUserByUserName(userName));
		return "changePassword";
	}
	
	@RequestMapping(path="/users/{userName}/changePassword", method=RequestMethod.POST)
	public String changePassword(ModelMap model, @PathVariable String userName, @RequestParam String password) {
		User user = new User();
		userDAO.updatePassword(userName, password);
		user.setUserName(userName);
		if(user.getPermission().equals("0")) {
			model.put("user", user);
			return "staffDashboard";
		}
		else if(user.getPermission().equals("1")) {
			model.put("user", user);
			return "studentDashboard";
		}
		else {
			model.put("user", user);
			return "employerDashboard";
		}
	}
	
	@RequestMapping(path="/users/{userName}/searchStudents", method=RequestMethod.GET)
	public String searchStudentsForm(ModelMap model) {
		List<String> languages = new ArrayList<>();
		List<Cohort> cohorts = userDAO.getCohorts();
		List<String> techSkills= userDAO.getAllTechnicalSkills(); 
		for(Cohort cohort : cohorts) {
			if(!languages.contains(cohort.getProgrammingLang())) {
				languages.add(cohort.getProgrammingLang());
			}
		}
		model.put("techSkills", techSkills);
		model.put("languages", languages);
		return "searchStudents";
	}
	
	@RequestMapping(path="/users/{userName}/searchStudents", method=RequestMethod.POST)
	public String searchStudents(ModelMap model, String language, Education targetEducation, String techSkills) {
		List<Student> studentResults = new ArrayList<>();
		List<Student> allStudents = userDAO.getSearchableStudentsByLanguage(language);
		List<String> techSkillCollection = new ArrayList<>();
		if(techSkills != null) {
			for(String techSkill : techSkills.split(",")) {
				techSkillCollection.add(techSkill);
			}
		}
		for(Student student : allStudents) {
			boolean hasTechSkills = false;
			boolean hasDegree = false;
			if(techSkillCollection.size() > 0) {
				hasTechSkills = student.getTechSkills().containsAll(techSkillCollection);
			} else {
				hasTechSkills = true;
			}
			if(Integer.parseInt(targetEducation.getDegree()) == 0) {
				hasDegree = true;
			} else {
				for(Education education : student.getEducation()) {
					if(Integer.parseInt(education.getDegree()) >= Integer.parseInt(targetEducation.getDegree())) {
						hasDegree = true;
					}
				}
			}
			if(hasTechSkills && hasDegree) {
				studentResults.add(student);
			}
		}
		model.put("studentResults", studentResults);
		return "searchStudentsResults";
	}
}
