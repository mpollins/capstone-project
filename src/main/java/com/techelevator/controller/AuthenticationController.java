package com.techelevator.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.model.UserDAO;

@Controller
@SessionAttributes({"currentUser", "user"})
public class AuthenticationController {

	private UserDAO userDAO;

	@Autowired
	public AuthenticationController(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@RequestMapping(path="/login", method=RequestMethod.GET)
	public String displayLoginForm() {
		return "login";
	}
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public String login(ModelMap model, @RequestParam String password, @RequestParam String userName,
						HttpSession session) {
		if(userDAO.searchForUsernameAndPassword(userName, password)) {
			session.invalidate();
			model.put("currentUser", userName);
			model.put("user", userDAO.getUserByUserName(userName));
//			if(isValidRedirect(destination)) {
//				return "redirect:"+ destination;
//			} else {
//				return "redirect:/users/"+ user.getUserName();
//			}
			return "redirect:/users/"+ userName;
		} else {
			return "redirect:/login";
		}
	}

//	private boolean isValidRedirect(String destination) {
//		return destination != null && destination.startsWith("http://localhost");
//	}

	@RequestMapping(path="/logout", method=RequestMethod.POST)
	public String logout(ModelMap model, HttpSession session) {
		model.remove("currentUser");
		model.remove("user");
		model.clear();
		session.removeAttribute("currentUser");
		session.removeAttribute("user");
		return "redirect:/home";
	}
}
