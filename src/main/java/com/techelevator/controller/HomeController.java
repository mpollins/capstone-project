package com.techelevator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.NewUser;
import com.techelevator.model.UserDAO;

@Controller
@Transactional
public class HomeController {
	
	private UserDAO userDAO;

	@Autowired
	public HomeController(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	@RequestMapping(path={"/", "/home"}, method=RequestMethod.GET)
	public String showHomePage(ModelMap model) {
		return "home";
	}
	
	@RequestMapping(path="/app", method=RequestMethod.GET)
	public String showWeCanCodeIt(ModelMap model) {
		return "weCanCodeIt";
	}
	
	@RequestMapping(path="/newUserAccountConfirm{confirmationSalt}", method=RequestMethod.GET)
	public String accountConfirm(ModelMap model, @PathVariable String confirmationSalt) {
		userDAO.updateSetUserActive(confirmationSalt);
		return "redirect:/login";
	}
	
//	@RequestMapping(path="/newUser", method=RequestMethod.GET)
//	public String addNewUserPage(ModelMap model) {
//		for(Cohort each : userDAO.getCohorts()) {
//			System.out.println(each.getLocation());
//			System.out.println(each.getCohortId());
//		}
//		model.put("cohortList", userDAO.getCohorts());
//		return "newUser";
//	}
	
	@RequestMapping(path="/cohortAnon{city}", method=RequestMethod.GET)
	public String displayClevelandCohortStudents(ModelMap model, @PathVariable String city) {
		model.put("city", city);
		model.put("cohort", userDAO.getPublicStudents(city, true));
		return "cohortAnon";
	}
	
//	@RequestMapping(path="/studentProfile", method=RequestMethod.GET)
//	public String viewStudentProfile(ModelMap model, @RequestParam int studentId) {
//		Student student = userDAO.getStudentById(studentId);
//		model.put("student", student);
//		return "studentProfileDetailed";
//	}
	
//	@RequestMapping(path="/editStudentProfile", method=RequestMethod.GET)
//	public String editStudentProfile(ModelMap model) {
//		int userId = 1;
//		model.put("student", userDAO.getStudentByUserId(userId));
//		return "editStudentProfile";
//	}
	
//	@RequestMapping(path="/cohortUserView{city}", method=RequestMethod.GET)
//	public String viewCohortStudentsUser(ModelMap model, @PathVariable String city) {
//		model.put("cohort", userDAO.getPublicStudents(city));
//		model.put("city", city);
//		return "cohortUserView";
//	}
}
