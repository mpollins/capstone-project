$(document).ready(function () {
  		$("#changePasswordForm").validate( {
  			debug: false,
  			submitHandler: function(form) {
  			    form.submit();
  			},
  			
  			rules: {
  				password: {
  					required: true,
					minlength: 10,
//  				maxlength: 128,
  					complexPassword: true,
  					noMoreThan2Duplicates: true
  				},
  				confirmPassword: {
  					required: true,
  					equalTo: "#password"
  				}
  			},

  			messages: {
  				password: {
  					required: "Please enter a password.",
  					minlength: "Password must be at least 10 characters.",
  					maxlength: "Password must not exceed 128 characters.",
  					complexPassword: "Please see Password Rules.",
  					noMoreThan2Duplicates: "Please choose another password."
  				},
				confirmPassword : {
					required: "Please re-enter password.",
					equalTo: "Passwords do not match"
				}
  			},
  			errorClass: "error",
  			validClass: "valid"
  		});
});

//
//<script type="text/javascript">
//$(document).ready(function () {
//
//	$("#changePasswordForm").validate({
//		
//		rules : {
//			password : {
//				required : true,
//				minlength : 10,
//				maxlength : 128,
//				complexPassword : true,
//				noMoreThan2Duplicates : true
//			},
//			confirmPassword : {
//				required : true,		
//				equalTo : "#password"  
//			}
//		},
//		messages : {			
//			confirmPassword : {
//				equalTo : "Passwords do not match"
//			}
//		},
//		errorClass : "error"
//	});
//});
//</script>









