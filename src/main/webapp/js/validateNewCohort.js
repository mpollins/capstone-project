$(document).ready(function () {
  		$("#newCohortForm").validate( {
  			debug: false,
  			submitHandler: function(form) {
  			    form.submit();
  			},
  			
  			rules: {
  				location: {
  					required: true,
  					alpha: true
  				},
  				programmingLang: {
  					required: true
  				},
  				cohortNumber: {
  					required: true,
  					numeric: true
  				}
  			},

  			messages: {
  				location: {
  					required: "Please enter a location.",
  					alpha: "Please use letters only."
  				},
  				programmingLang: {
					required: "Please enter a programming language."},
				cohortNumber: {
					required: "Please enter a cohort number.",
					numeric: "Please use numbers only."
				}
  			},
  			errorClass: "error",
  			validClass: "valid"
  		});
});


$.validator.addMethod("alpha", function(value, index) {
    return value.match(/^[a-zA-Z]*$/);
 }, "Please use letters only.");

$.validator.addMethod("numeric", function(value, index) {
    return value.match(/^[\d]*$/);
 }, "Please use numbers only.");









