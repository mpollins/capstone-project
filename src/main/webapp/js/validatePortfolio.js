$(document).ready(function () {
  		$("#newProjectForm").validate( {
  			debug: false,
  			submitHandler: function(form) {
  			    form.submit();
  			},
  			
  			rules: {
  				projectName: {
  					required: true
  				},
  				link: {
					url: true
  				}
  			},

  			messages: {
  				projectName: {
					required: "Please enter the name of your project."
				},
				link: {
					url: "Please enter a valid URL."
				}
  			},
  			errorClass: "error",
  			validClass: "valid"
  		});
});

$.validator.addMethod("validName", function(value, index) {
    return value.match(/^[a-zA-Z -]*$/);
 }, "Please use letters, spaces and dashes only.");










