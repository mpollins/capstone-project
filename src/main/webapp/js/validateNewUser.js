$(document).ready(function () {
  		$("#newUserForm").validate( {
  			debug: false,
  			submitHandler: function(form) {
  			    form.submit();
  			},
  			
  			rules: {
  				email: {
  					required: true,
  					email: true
  				},
  				firstName: {
  					required: true,
  					validName: true
  				},
  				lastName: {
  					required: true,
  					validName: true
  				},
  				userName: {
  					required: true,
  					alpha: true
  				},
  				password: {
  					required: true,
					minlength: 10,
//  				maxlength: 128,
  					complexPassword: true,
  					noMoreThan2Duplicates: true
  				},
  				confirmPassword: {
  					required: true,
  					equalTo: "#password"
  				}
  			},

  			messages: {
  				email: {
  					required: "Please enter an email address.",
  					email: "Please enter a valid email address."
  				},
  				firstName: {
					required: "Please enter your first name.",
					validName: "Please use letters, spaces and dashes only.",
					maxlength: "Your name must not exceed 25 characters."
				},
				lastName: {
					required: "Please enter your last name.",
					validName: "Please use letters, spaces and dashes only."
				},
				userName: {
  					required: "Please enter a username.",
  					alpha: "Please use letters only."
  				},
  				password: {
  					required: "Please enter a password.",
  					minlength: "Password must be at least 10 characters.",
  					maxlength: "Password must not exceed 128 characters.",
  					complexPassword: "Please see Password Rules.",
  					noMoreThan2Duplicates: "Please choose another password."
  				},
				confirmPassword : {
					required: "Please re-enter password.",
					equalTo: "Passwords do not match"
				}
  			},
  			errorClass: "error",
  			validClass: "valid"
  		});
});

$.validator.addMethod("validName", function(value, index) {
    return value.match(/^[a-zA-Z -]*$/);
 }, "Please use letters, spaces and dashes only.");

$.validator.addMethod("alpha", function(value, index) {
    return value.match(/^[a-zA-Z]*$/);
 }, "Please use letters only.");










