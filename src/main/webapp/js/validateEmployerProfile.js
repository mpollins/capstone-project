$(document).ready(function () {
  		$("#newUserForm").validate( {
  			debug: false,
  			submitHandler: function(form) {
  			    form.submit();
  			},
  			
  			rules: {
  				businessName: {
  					required: true
  				},
  				firstName: {
  					required: true,
					validName: true
  				},
  				lastName: {
  					required: true,
					validName: true
  				}
  			},

  			messages: {
				businessName: {
					required: "Please enter the name of your business."
				},
  				firstName: {
					required: "Please enter your first name.",
					validName: "Please use letters, spaces and dashes only."
				},
				lastName: {
					required: "Please enter your last name.",
					validName: "Please use letters, spaces and dashes only."
				}
  			},
  			errorClass: "error",
  			validClass: "valid"
  		});
});

$.validator.addMethod("validName", function(value, index) {
    return value.match(/^[a-zA-Z -]*$/);
 }, "Please use letters, spaces and dashes only.");











