// Floating label headings for the contact form
$(function() {
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
    });
});


// Navigation Scripts to Show Header on Scroll-Up
jQuery(document).ready(function($) {
    var MQL = 1170;
    $('#footer').html('Copyright &copy; Tech Elevator ' + new Date().getFullYear());
    
    //primary navigation slide-in effect
    if ($(window).width() > MQL) {
        var headerHeight = $('.navbar-custom').height();
        $(window).on('scroll', {
                previousTop: 0
            },
            function() {
                var currentTop = $(window).scrollTop();
                //check if user is scrolling up
                if (currentTop < this.previousTop) {
                    //if scrolling up...
                    if (currentTop > 0 && $('.navbar-custom').hasClass('is-fixed')) {
                        $('.navbar-custom').addClass('is-visible');
                    } else {
                        $('.navbar-custom').removeClass('is-visible is-fixed');
                    }
                } else if (currentTop > this.previousTop) {
                    //if scrolling down...
                    $('.navbar-custom').removeClass('is-visible');
                    if (currentTop > headerHeight && !$('.navbar-custom').hasClass('is-fixed')) $('.navbar-custom').addClass('is-fixed');
                }
                this.previousTop = currentTop;
            });
    }
});

$(document).ready(function() {
    $(".btn-pref .btn").click(function () {
        $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
        // $(".tab").addClass("active"); // instead of this do the below 
        $(this).removeClass("btn-default").addClass("btn-primary");   
    });
    });


$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#permission").change(function () {
        toggleFields();
    });
});

//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
    if ($("#permission").val() == "1"){
        $(".employer-form").hide();
        $(".student-form").show();
    }
    else if ($("#permission").val() == "2") {
        $(".student-form").hide();
        $(".employer-form").show();
    }
    else {
        $(".student-form").hide();
        $(".employer-form").hide();
    }
}

$(document).ready(function() {
    $("#profile-div").change(function() {
        toggleFormField();
    });
});

$(document).ready(function() {
    $(".glyphicon-ok").hide();
    toggleFormField();
    checkTextChange();
    toggleDiscoverable();
    $('#delete').on('click', function() {
    	$('#deleteField').val('true');
    });
});

function toggleFormField() {
	$('.edit').unbind('click');
    $('.edit').on("click", function (e) {
    	if($(this).next('input').prop("readonly")) {
    		$(this).next('input').prop("readonly", false);
    	}
    	else {
    		$(this).next('input').prop("readonly", true);
    	}
    	
    	if($(this).next('textarea').prop("readonly")) {
    		$(this).next('textarea').prop("readonly", false);
    	}
    	else {
    		$(this).next('textarea').prop("readonly", true);
    	}
    });
}

function checkTextChange() {
    $('input').change(function() {
        $(this).next('span').show();
    });
    $('textarea').change(function() {
        $(this).next('span').show();
    });
}

function toggleDiscoverable() {
	$('.discoverable-toggle').unbind('click');
	$('.discoverable-toggle').click(function(e) {
		if($(this).next('input').val() == 'false') {
			$(this).next('input').val('true');
			
			$(this).next().next().show();
		}
		else {
			$(this).next('input').val('false');
			$(this).next().next().hide();
		}
	});
}

$(document).ready(function () {
    $('.form-container').hide();
    $('.portfolio-form-container').hide();
    $('.cohort-box').hide();
    toggleExperienceFields();
	toggleEducationFields();
	togglePortfolioFields();
	toggleSocialMediaForms();
	toggleSocialMediaFields();
	toggleCohorts();
	
	$('#cohortButton').change(function() {
		$('.cohort-box').hide();
		toggleCohorts();
	});
	
	$('#socialMediaButtonLower').change(function () {
		toggleSocialMediaFields();
	});
	
	$('#socialMediaButton').change(function () {
		$('.form-container').hide();
		toggleSocialMediaForms();
	});
    $("#schoolButton").change(function () {
    	$('.form-container').hide();
        toggleEducationFields();
    });
    $("#experienceButton").change(function () {
    	$('.form-container').hide();
        toggleExperienceFields();
    });
    $("#portfolioButton").change(function () {
    	$('.portfolio-form-container').hide();
    	togglePortfolioFields();
    });
});

function toggleCohorts() {
	var cohortName = $('#cohortButton').val();
	$('.' + cohortName).show();
}

function toggleSocialMediaFields() {
	var fieldName = $('#socialMediaButtonLower').val();
	$('#smName').val(fieldName);
}

function toggleSocialMediaForms() {
	var formId = $('#socialMediaButton').val();
	$('#form' + formId).show();
}

function togglePortfolioFields() {
	var formId = $('#portfolioButton').val();
	$('#form' + formId).show();
}

function toggleExperienceFields() {
	var formId = $('#experienceButton').val();
	$('#form' + formId).show();
}

function toggleEducationFields() {
	var formId = $('#schoolButton').val();
	$('#form' + formId).show();
}

$(document).ready(function() {
	$('.hiddenSelectTech').hide();
	$('.hiddenSelectSoft').hide();
	$('.techSkill:checked').each(function( index ) {
		  $('#selectTechSkills').append("<option selected='selected' value='" + $(this).attr('name') + "' id='" + $(this).attr('id') + "'>" + $(this).attr('name') + "</option>");
	});
	$('.softSkill:checked').each(function( index ) {
		  $('#selectSoftSkills').append("<option selected='selected' value='" + $(this).attr('name') + "' id='" + $(this).attr('id') + "'>" + $(this).attr('name') + "</option>");
	});
	$('input.techSkill:checkbox').change(
		    function(){
		        if ($(this).is(':checked')) {
		        	$('#selectTechSkills').append("<option selected='selected' value='" + $(this).attr('name') + "' id='" + $(this).attr('id') + "'>" + $(this).attr('name') + "</option>");
		        }
		        else {
		        	var id = '#' + $(this).attr('id');
		        	$('#selectTechSkills > ' + id).remove();
		        }
		    });
	$('input.softSkill:checkbox').change(
		    function(){
		        if ($(this).is(':checked')) {
		        	$('#selectSoftSkills').append("<option selected='selected' value='" + $(this).attr('name') + "' id='" + $(this).attr('id') + "'>" + $(this).attr('name') + "</option>");
		        }
		        else {
		        	var id = '#' + $(this).attr('id');
		        	$('#selectSoftSkills > ' + id).remove();
		        }
		    });
});

//$(document).ready(function() {
//	var lengthOfList = $('#softSkillsList').children().length;
//	$( "#softSkillsList" ).on( "click", "li", function( event ) {
////	    event.preventDefault();
//		$(this).remove();
//		lengthOfList--;
//		console.log(lengthOfList);
//	});
//	$('#addSkillButton').click(function() {
//		var value = $('#addSkillInput').val();
//		$('#softSkillsList').append("<li class='list-group-item' id='" + lengthOfList + "'><button class='btn btn-default btn-block' type='button'>" + value + "</button></li>");
//		lengthOfList++;
//		$('#addSkillInput').val('');
//		console.log(lengthOfList);
//	});
//});