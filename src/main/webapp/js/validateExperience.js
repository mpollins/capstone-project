$(document).ready(function () {
  		$("#newExperienceForm").validate( {
  			debug: false,
  			submitHandler: function(form) {
  			    form.submit();
  			},
  			
  			rules: {
  				company: {
  					required: true,
					validName: true
  				},
  				title: {
  					required: true,
					validName: true
  				}
  			},

  			messages: {
  				company: {
					required: "Please enter employer's name.",
					validName: "Please use letters, spaces and dashes only."
				},
				title: {
					required: "Please enter your job title.",
					validName: "Please use letters, spaces and dashes only."
				}
  			},
  			errorClass: "error",
  			validClass: "valid"
  		});
});

$.validator.addMethod("validName", function(value, index) {
    return value.match(/^[a-zA-Z -]*$/);
 }, "Please use letters, spaces and dashes only.");










