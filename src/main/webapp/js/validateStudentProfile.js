$(document).ready(function () {
  		$("#newUserForm").validate( {
  			debug: false,
  			submitHandler: function(form) {
  			    form.submit();
  			},
  			
  			rules: {
  				firstName: {
  					required: true,
					validName: true
  				},
  				lastName: {
  					required: true,
					validName: true
  				},
  				email: {
  					required: true,
  					email: true
//					validEmail: true
  				},
  				address1: {
  					alphanumeric: true
  				},
  				address2: {
  					alphanumeric: true,
  					validAddress: true
  				},
				city: {
					alpha: true
				},
				state: {
					alpha: true
				},
				zip: {
					numeric: true
				},
				phone: {
					phoneFormat: true
				}
  			},

  			messages: {
				firstName: {
					required: "Please enter your first name.",
					validName: "Please use letters, spaces and dashes only."
				},
				lastName: {
					required: "Please enter your last name.",
					validName: "Please use letters, spaces and dashes only."
				},
 				email: {
  					required: "Please enter an email address.",
  					email: "Please enter a valid email address."
  				},
  				address1: {
					alphanumeric: "Please use letters, numbers and spaces only."
  				},
  				address2: {
  					alphanumeric: "Please use letters, numbers and spaces only.",
  					validAddress: "May not be filled if first Address line is blank."	
  				},
				city: {
					alpha: "Please use letters and spaces only."
				},
				state: {
					alpha: "Please use letters and spaces only."
				},
				zip: {
					numeric: "Please use numbers and dashes only."
				},
				phone: {
					phoneFormat: "Required format is (999)999-9999 or 999-999-9999."
				}
  			},
  			errorClass: "error",
  			validClass: "valid"
  		});
});

$.validator.addMethod("validName", function(value, index) {
    return value.match(/^[a-zA-Z -]*$/);
 }, "Please use letters, spaces and dashes only.");

//$.validator.addMethod("validEmail", function (value, index) {
//    return value.toLowerCase().match(/^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i);   
//}, "Please enter a valid email address.");

$.validator.addMethod("alpha", function(value, index) {
    return value.match(/^[a-zA-Z ]*$/);
 }, "Please use letters and spaces only.");

$.validator.addMethod("numeric", function(value, index) {
    return value.match(/^[\d -]*$/);
 }, "Please use numbers and dashes only.");

$.validator.addMethod("alphanumeric", function(value, index) {
    return value.match(/^[a-zA-Z\d ]*$/);
 }, "Please use letters, numbers and spaces only.");
//return value.match(/[A-Z]/) && value.match(/[a-z]/) && value.match(/\d/); 

$.validator.addMethod("validAddress", function(value, index) {
	return $("input[name=address2]").val().length==0 || $("input[name=address1]").val().length > 0;
//	return $("#address2").val().length==0 || $("#address1").val().length > 0;
//	return $("input[name=address1]").length > 0 && $("input[name=address1]").val() != "";
}, "May not be filled if first Address line is blank.");

$.validator.addMethod("phoneFormat", function (value, index) {
	return value.match(/^(\(\d{3}\)\s?\d{3}-\d{4})$/) || value.match(/^(\d{3}-\s?\d{3}-\d{4})$/);
}, "Required format is (999)999-9999 or 999-999-9999.");








