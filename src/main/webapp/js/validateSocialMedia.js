$(document).ready(function () {
  		$("#newForm").validate( {
  			debug: false,
  			submitHandler: function(form) {
  			    form.submit();
  			},
  			
  			rules: {
//  				smName: {
//  					required: true,
//					validName: true
//  				},
  				link: {
  					required: true,
					url: true
  				}
  			},

  			messages: {
//				smName: {
//					required: "Please enter social media name.",
//					validName: "Please use letters, spaces and dashes only."
//				},
				link: {
					required: "Please enter link.",
					url: "Please enter a valid URL."
				}
  			},
  			errorClass: "error",
  			validClass: "valid"
  		});
});

//$.validator.addMethod("validName", function(value, index) {
//    return value.match(/^[a-zA-Z -]*$/);
// }, "Please use letters, spaces and dashes only.");










