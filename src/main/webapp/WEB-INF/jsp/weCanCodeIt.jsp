<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Tech Elevator Student Profiles</title>
		<c:url var="bootstrapHref" value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="stylesheet" href="${bootstrapHref}">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<c:url var="jQueryMinHref" value="/js/jquery.min.js" />
	    <script src="${jQueryMinHref}"></script>
	    <c:url var="jQueryValidateHref" value="/js/jquery.validate.min.js" />
	    <script src="${jQueryValidateHref}"></script>
	    <c:url var="jQueryMethodsHref" value="/js/additional-methods.min.js" />
	    <script src="${jQueryMethodsHref}"></script>
	    <script src="https://cdn.jsdelivr.net/jquery.timeago/1.4.1/jquery.timeago.min.js"></script>
	    <c:url var="bootstrapJsHref" value="/js/bootstrap.min.js" />
	    <script src="${bootstrapJsHref}"></script>
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css">
	    <c:url var="cssHref" value="/css/site.css" />
		<link rel="stylesheet" type="text/css" href="${cssHref}">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>
		
	</head>
	<body>
		<header><!-- Link Referencecs -->
			<c:url var="homePageHref" value="http://www.techelevator.com"/>
			<c:url var="homeLinkHref" value="/home"/>
			<c:url var="imgSrc" value="http://static1.squarespace.com/static/55ef2da9e4b03f6e1ef0cd28/t/57ae02eeebbd1aaf598d3b32/1480957490797/?format=1500w" />
			<c:url var="cohortClePublic" value="/cohortAnonCleveland" />
			<c:url var="cohortColPublic" value="/cohortAnonColumbus" />
			<c:url var="storyHref" value="http://www.techelevator.com/student-success/" />
			<c:url var="dashboardHref" value="/users/${currentUser}" />
			<c:url var="cohortCleHref" value="/users/${currentUser}/cohortUserViewCleveland" />
			<c:url var="cohortColHref" value="/users/${currentUser}/cohortUserViewColumbus" />
			<c:url var="personalDetailsHref" value="/users/${currentUser}/editProfile" />
			<c:url var="changePasswordHref" value="/users/${currentUser}/changePassword"/>
			<c:url var="eventsHref" value="https://techelevatorcbus_showcases.eventbrite.com/" />
			<c:url var="workHref" value="/users/${currentUser}/editExperience" />
			<c:url var="educationHref" value="/users/${currentUser}/editEducation" />
			<c:url var="skillsHref" value="/users/${currentUser}/editSkills" />
			<c:url var="socialMediaHref" value="/users/${currentUser}/editSocialMedia" />
			<c:url var="portfolioHref" value="/users/${currentUser}/editPortfolio" />
			<c:url var="personalProfileHref" value="/users/${currentUser}/studentProfile?studentId=${user.userId}" />
			<c:url var="techStackHref" value="/users/${currentUser}/editTechStack" />
			<c:url var="viewAllEmployersHref" value="/users/${currentUser}/viewAllEmployers" />
			<c:url var="searchStudentHref" value="/users/${currentUser}/searchStudents" /> 
		</header>
		<nav class="navbar navbar-default navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a href="https://wecancodeit.org/wp-content/uploads/2016/07/WCCIT-logo-180x28.png" class="navbar-brand hidden-xs"><img id="logo" src="https://wecancodeit.org/wp-content/uploads/2016/07/WCCIT-logo-180x28.png" alt="Brand" style="height: 60px;"/></a>
			      <a href="https://wecancodeit.org/wp-content/uploads/2016/07/WCCIT-logo-180x28.png" class="navbar-brand visible-xs"><img id="logo" src="https://wecancodeit.org/wp-content/uploads/2016/07/WCCIT-logo-180x28.png" alt="Brand" style="height: 60px;"/></a>
			    </div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			     <ul class="nav navbar-nav">
			     <!-- NavBar to be displayed for anonymous users -->
						<li><a href="${homeLinkHref}">Home</a></li>
						<li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cohorts <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="${cohortColPublic}">Columbus</a></li>
				            <li><a href="${cohortClePublic}">Cleveland</a></li>
				          </ul>
				        </li>
				        <li><a href="${storyHref}">Student Stories</a></li>
				        <li><a href="http://www.techelevator.com/contact">Contact Us</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<c:choose>
							<c:when test="${empty currentUser}">
								<c:url var="loginHref" value="/login" />
								<li><a href="${loginHref}">Log In</a></li>
							</c:when>
							<c:otherwise>
								<c:url var="logoutAction" value="/logout" />
								
								<form id="logoutForm" action="${logoutAction}" method="POST">
									<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
								</form>
								<li><a id="logoutLink" href="#">Log Out</a></li>
							</c:otherwise>
						</c:choose>
					</ul>
					
			    </div><!-- /.navbar-collapse -->
			</div>
		</nav>	
		<div class="container">

<header class="intro-header" id="header-img"
	style="background-image: url('https://static1.squarespace.com/static/55ef2da9e4b03f6e1ef0cd28/t/579bb286e58c62582a241cf4/1470059580936/Columbus-OH-Skyline.jpg?format=1500w');">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<div class="site-heading">
					<h1>We Can Code It</h1>
					<hr class="small">
					<br>
					<span class="subheading">Like Tech Elevator But Without Awesome Beards</span>
						<br>
						<br>
						<br>
				<span class="subheading"><a href="http://www.techelevator.com/apply" style="color:white;text-decoration:none">Apply Now</a></span> 
				</div>
			</div>
		</div>
	</div>
</header>
		</div>
		<div id="spacing"></div>
		<footer class="text-center navbar-fixed-bottom" id="footer">
		 <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <p class="text-muted" id="footer-text">Copywrite We Can Code It 2016</p>
                </div>
            </div>
        </div>
		</footer>
		<c:url var="ourJsHref" value="/js/script.js" />
	    <script src="${ourJsHref}"></script>
	</body>
</html>
