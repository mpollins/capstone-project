<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/header.jsp" />

<script type="text/javascript">
	$(document).ready(function () {
	
		$("form").validate({
			
			rules : {
				userName : {
					required : true
				},
				password : {
					required : true
				}
			},
			messages : {			
				confirmPassword : {
					equalTo : "Passwords do not match"
				}
			},
			errorClass : "error"
		});
	});
</script>

<div class="row">
	<div class="col-sm-6 col-sm-offset-3"><h1>New User Account Validation Email</h1></div>
</div>

<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Name</h3>
	  </div>
	  <div class="panel-body">
	    ${newUser.firstName} ${newUser.lastName}
	  </div>
	</div>
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Username</h3>
	  </div>
	  <div class="panel-body">
	    ${newUser.userName}
	  </div>
	</div>
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Password</h3>
	  </div>
	  <div class="panel-body">
	    ${password}
	  </div>
	</div>
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Email</h3>
	  </div>
	  <div class="panel-body">
	    ${newUser.email}
	  </div>
	</div>
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Email Body</h3>
	  </div>
	  <div class="panel-body">
	  <c:choose>
			<c:when test="${newUser.permission eq 0}">
				${newUser.firstName},<br/>I have created an account for you on the Tech Elevator Super Student Profile BuilderApp&trade;.  Please take time to make sure all the information for your cohort is up to date and accurate.<br/>Thanks, Terry Warnock<br/>Columbus Grand Master
			</c:when>
			<c:when test="${newUser.permission eq 1}">
				${newUser.firstName},<br/>Welcome to Tech Elevator! Our Super Student Profile Builder App&trade; is the perfect place for you to connect with other Tech Elevator students and employers as well.  Follow this link to activate your account and have fun!<br/>Thanks, <br/>Terry Warnock<br/>Super Director Extraordinaire
			</c:when>
			<c:otherwise>
				${newUser.firstName},<br>Thank you for showing support in our program, we really value having a relationship with your organization. We created this system to allow you better access to our students and to create a direct channel for communication.<br><br>Terry Warnock<br>Tech Elevator
			</c:otherwise>
		</c:choose>
	  </div>
	</div>
		<c:choose>
			<c:when test="${newUser.permission eq 0}">
				<a href="mailto:${newUser.email}?subject=Join%20Us&body=${newUser.firstName}%2C%0AI%20have%20created%20an%20account%20for%20you%20on%20the%20Tech%20Elevator%20Super%20Student%20Profile%20BuilderApp%E2%84%A2.%20%20Please%20take%20time%20to%20make%20sure%20all%20the%20information%20for%20your%20cohort%20is%20up%20to%20date%20and%20accurate.%0AThanks%2C%20%0ATerry%20Warnock%0AColumbus%20Grand%20Master%0A%0A${confirmationLink}%0APassword%20-%20${password}%0AUsername%20-%20${newUser.userName}"><button type="button" class="btn btn-default">Send Email</button></a>
			</c:when>
			<c:when test="${newUser.permission eq 1}">
				<a href="mailto:${newUser.email}?subject=Join%20Us&body=${newUser.firstName}%2C%0AWelcome%20to%20Tech%20Elevator!%20Our%20Super%20Student%20Profile%20Builder%20App%E2%84%A2%20is%20the%20perfect%20place%20for%20you%20to%20connect%20with%20other%20Tech%20Elevator%20students%20and%20employers%20as%20well.%20%20Follow%20this%20link%20to%20activate%20your%20account%20and%20have%20fun!%0AThanks%2C%20%0ATerry%20Warnock%0ASuper%20Director%20Extraordinaire%0A%0A${confirmationLink}%0APassword%20-%20${password}%0AUsername%20-%20${newUser.userName}"><button type="button" class="btn btn-default">Send Email</button></a>
			</c:when>
			<c:otherwise>
				<a href="mailto:${newUser.email}?subject=Join%20Us&body=${newUser.firstName}%2C%0AThank%20you%20for%20showing%20support%20in%20our%20program%2C%20we%20really%20value%20having%20a%20relationship%20with%20your%20organization.%20We%20created%20this%20system%20to%20allow%20you%20better%20access%20to%20our%20students%20and%20to%20create%20a%20direct%20channel%20for%20communication.%0ATerry%20Warnock%0ATech%20Elevator%0A%0A${confirmationLink}%0APassword%20-%20${password}%0AUsername%20-%20${newUser.userName}"><button type="button" class="btn btn-default">Send Email</button></a>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="col-sm-4"></div>
</div>
<br/>
<c:import url="/WEB-INF/jsp/footer.jsp" />