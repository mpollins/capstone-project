<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:url var="formAction" value="/users/${userName}/newCohort" />
<form method="POST" action="${formAction}" id="newCohortForm">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<div class="row">
		<div class="col-sm-6">	
		
			<div class="form-group">
				<label for="location">Location: </label>
				<input type="text" id="location" name="location" maxlength="25" placeHolder="City" class="form-control" />	
			</div>
			<div class="form-group">
				<label for="programmingLang">Language: </label>
				<input type="text" id="programmingLang" name="programmingLang" maxlength="10" placeHolder="Programming Language" class="form-control" />	
			</div>
			<div class="form-group">
				<label for="cohortNumber">Cohort Number: </label>
				<input type="number" id="cohortNumber" name="cohortNumber" maxlength="3" placeHolder="Cohort Number" class="form-control" />	
			</div>
			
			<button type="submit" class="btn btn-default">Create Cohort</button>
		</div>
	</div>
</form>
<br>
<c:url var="formAction" value="/users/${userName}/deleteCohort" />
<form method="POST" action="${formAction}" id="deleteTechSkillForm">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<div class="row">
		<div class="col-sm-6">	
		
			<div class="form-group">
			 <label for="cohort">Cohorts: </label>
			 <select class="selectpicker form-control" name="techSkill" data-width="auto"  data-size="6">
		 	  <c:forEach  var="cohort" items="${ cohorts }">
		 	 	<option value="${ cohort.cohortId }">${ cohort.location } ${ cohort.programmingLang }[${ cohort.cohortNumber }]</option>
			  </c:forEach> 
			 </select>
			</div>	
			
			<button type="submit" class="btn btn-default">Delete Cohort</button>
		</div>
	</div>
</form>
		
<c:url var="jsHref" value="/js/validateNewCohort.js"/>
<script src="${jsHref}"></script>
<c:import url="/WEB-INF/jsp/footer.jsp" />