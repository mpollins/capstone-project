<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var="year" value="${now}" pattern="yyyy" />
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="/WEB-INF/jsp/header.jsp" />
<c:set var="listLength" value="${fn:length(student.socialMedia)}"/>
<c:set var="fullListLength" value="${fn:length(allSocialMedia)}"/>

<!-- HEADER -->
<div class="row">
	<div class="col-sm-3"></div>
	<div class="col-sm-6 text-center"><h1>Edit Your Social Media Accounts</h1></div>
	<div class="col-sm-3"></div>
</div>

<!-- Button dynamically generated with all education entries -->
<div class="row">
  <div class="col-sm-4"></div>
  <div class="col-sm-4 text-center">
  <div class="form-group">
  	<select class="selectpicker form-control" id="socialMediaButton" data-width="auto">
	  <option value="NewSocialMedia">New Social Media Account</option>
	  <c:if test="${listLength gt 0}">
	 	 <c:forEach  var="socialMediaItems" items="${student.socialMedia}">
	  		<option value="${socialMediaItems.socialMediaId}">${socialMediaItems.name}</option>
	 	 </c:forEach> 
	  </c:if>
	</select>
	</div>
  </div>
  <div class="col-sm-4"></div>
</div>
<br/>

<c:url var="formAction" value="/users/${userName}/editSocialMedia" />
<!-- Forms for each social media entry dynamically generated -->
<c:if test="${listLength gt 0}">
<c:forEach  var="socialMediaItem" items="${student.socialMedia}">
<div class="row form-container" id="form${socialMediaItem.socialMediaId}">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
  
  <form method="POST" action="${formAction}" id="form${socialMediaItem.socialMediaId}">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
    <input type="hidden" name="userId" value="${student.userId}" />
    <input type="hidden" name="studentId" value="${student.studentId}" />
	<input type="hidden" name="socialMediaId" value="${socialMediaItem.socialMediaId}"/>
	<input id="deleteField" type="hidden" name="delete" value="false">

	<!-- Input fields -->
    <div class="input-group">
      <span class="input-group-btn">
        <label for="name" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Name</button></label>
      </span>
      <input type="text" class="form-control" value="${socialMediaItem.name}" name="name" maxlength="25" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="link" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Link</button></label>
      </span>
      <input type="text" class="form-control" value="${socialMediaItem.link}" name="link" maxlength="100" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    
    <br/>
    <!-- Submit Button -->
    <div class="col-sm-3"></div>
      <div class="col-sm-7 center-block">
        <button type="submit" class="btn btn-default" id="save">Save Changes</button>
        <button class="btn btn-danger" type="submit" id="delete">Delete Account</button>
      </div>
    <div class="col-sm-2"></div>
</form>
</div>
<div class="col-sm-2"></div>
</div>
</c:forEach>
</c:if>
<c:set var="notFound" value="${notPresent}"></c:set>
<c:if test="${fn:length(notFound) gt 0}">
<div class="row form-container" id="formNewSocialMedia">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
  
  <form method="POST" action="${formAction}" id="newForm">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
    <input type="hidden" name="userId" value="${student.userId}" />
    <input type="hidden" name="studentId" value="${student.studentId}" />
	<input type="hidden" name="socialMediaId" value="0"/>

	<!-- Input fields -->
	
	<div class="col-sm-4"></div>
	<div class="form-group col-sm-4 text-center">
  	<select class="selectpicker form-control" id="socialMediaButtonLower" data-width="auto">
	 	 <c:forEach  var="socialMediaType" items="${notPresent}">
	  		<option value="${socialMediaType}">${socialMediaType}</option>
	 	 </c:forEach>
	</select>
	</div>
	
    <div class="input-group col-sm-12">
      <span class="input-group-btn">
        <label for="name" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Name</button></label>
      </span>
      <input type="text" class="form-control" name="name" readonly="readonly" id="smName">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="link" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Link</button></label>
      </span>
      <input type="text" class="form-control" name="link">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    
    <br/>
    <!-- Submit Button -->
    <div class="col-sm-4"></div>
      <div class="col-sm-4 text-center">
        <button type="submit" class="btn btn-default" id="save">Save Changes</button>
      </div>
    <div class="col-sm-2"></div>
</form>
</div>
<div class="col-sm-2"></div>
</div>
</c:if>
<c:url var="jsHref" value="/js/validateSocialMedia.js"/>  
<script src="${jsHref}"></script>           
<c:import url="/WEB-INF/jsp/footer.jsp" />