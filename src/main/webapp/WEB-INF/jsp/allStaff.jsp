<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/header.jsp" />
<div class="row">
	<div class="col-sm-3"></div>
	<div class="col-sm-6 text-center">
		<h2>All Staff</h2>
	</div>
	<div class="col-sm-3"></div>
</div>

    <div class="row">
    <c:forEach var="staff" items="${allStaff}">
        	<div class="col-lg-3 col-sm-6">

            <div class="small-card small-hovercard">
                <div class="small-cardheader">

                </div>
                <div class="small-avatar">
                    <c:url var="avatarHref" value="/img/avatar.png" />
                    <img src="${avatarHref}" alt="avatar"/>
                </div>
                <div class="small-info">
                    <div class="small-title">
                        <p>${staff.firstName} ${staff.lastName}</p>
                    </div>
                </div>
                <div class="small-bottom">
                   <c:url var="userHref" value="/users/${currentUser}/userProfile"/>
                    <form action="${userHref}" method="GET"><input id="deleteField" type="hidden" name="delete" value="false"><input type="hidden" name="userId" value="${staff.userId}"><input type="hidden" name="staffId" value="${staff.staffId}"><button type="submit" class="btn btn-default">Profile</button> <button class="btn btn-danger" type="submit" id="delete">Delete</button></form>
                </div>
            </div>
        </div>
    </c:forEach>
    </div>

<c:url var="blurHref" value="/js/stackBlur.js" />
<script src="${blurHref}"></script>

<c:import url="/WEB-INF/jsp/footer.jsp" />
		