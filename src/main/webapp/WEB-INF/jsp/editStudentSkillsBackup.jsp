<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var="year" value="${now}" pattern="yyyy" />

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:set var="listLength" value="${student.education.size() - 1}"/>
<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>
<!-- HEADER -->
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4"><h1>Edit Your Skills</h1></div>
	<div class="col-sm-4"></div>
</div>
<br/>


<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="jumbotron">
                <!-- Default panel contents -->
                <div class="page-header text-center"><h1>Tech Skills</h1></div>
            
                <!-- List group // ID and For must match for the button to work properly-->
                <ul class="list-group">
                <c:set var="indexCount" value="0" scope="page"/>
                <c:forEach var="techSkill" items="${techSkillsList}">
                	<li class="list-group-item">
                        ${techSkill}
                        <div class="material-switch pull-right">
                            <input id="techSkill${indexCount}" name="${techSkill}" type="checkbox" checked="checked"/>
                            <label for="techSkill${indexCount}" class="label-default"></label>
                        </div>
                    </li>
                    <c:set var="indexCount" value="${indexCount + 1}" scope="page"/>
                </c:forEach>
                </ul>
                <div class="col-sm-2 center-block col-sm-offset-5">
            	<c:url var="formAction" value="/users/gsheeley/editSkills" />
            	<form method="POST" action="${formAction}" id="experienceForm">
            	<div class="hiddenSelectTech">
            		<select class="selectpicker show-tick" id="selectSkills" multiple data-selected-text-format="count" data-live-search="true" multiple data-width="100%">
            		</select>
            	</div>
		        	<button type="submit" class="btn btn-default" id="saveStudentSkills">Save Changes</button>
		        </form>
		    </div>
            </div> 
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="jumbotron">
        	<!-- Default panel contents -->
            <div class="page-header text-center"><h1>Soft Skills</h1></div>
            	
				    <div class="input-group">
				      <input type="text" class="form-control" placeholder="Add skill..." id="addSkillInput">
				      <span class="input-group-btn">
				        <button class="btn btn-default" type="button" id="addSkillButton">Add Skill</button>
				      </span>
				    </div><!-- /input-group -->
				  
        		<ul class="list-group" id="softSkillsList">
        			<li class="list-group-item" id="0"><button class='btn btn-default btn-block' type='button'>Public Speaking</button></li>
        			<li class="list-group-item" id="1"><button class='btn btn-default btn-block' type='button'>Cooking</button></li>
        			<li class="list-group-item" id="2"><button class='btn btn-default btn-block' type='button'>Running</button></li>
        		</ul>
        		<div class="col-sm-2 center-block col-sm-offset-5">
            	<c:url var="formActionSoft" value="/users/gsheeley/editSoftSkills" />
            	<form method="POST" action="${formActionSoft}">
            	<div class="hiddenSelectSoft">
            		<select class="selectpicker" id="selectSoftSkills" multiple data-selected-text-format="count" multiple data-width="100%">
            		</select>
            	</div>
		        	<button type="submit" class="btn btn-default" id="saveStudentSkills">Save Changes</button>
		        </form>
		    </div> 
    	    </div>
    	    
        </div>
    </div>
    
        
<c:import url="/WEB-INF/jsp/footer.jsp" />