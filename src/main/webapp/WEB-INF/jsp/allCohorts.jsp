<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/header.jsp" />
<div class="row">
	<div class="col-sm-3"></div>
	<div class="col-sm-6">
		<h2>All Cohorts</h2>
	</div>
	<div class="col-sm-3"></div>
</div>
    <div class="row">
    <c:forEach var="cohort" items="${allCohorts}">
        	<div class="col-lg-3 col-sm-6">

            <div class="small-card small-hovercard">
                <div class="small-info">
                    <div class="small-title">
                        <p>${cohort.location}</p>
                    </div>
                    <div class="small-desc"><strong>Programming Language: </strong> ${cohort.programmingLang}</div>
                    <div class="small-desc"><strong>Cohort Number: </strong> ${cohort.cohortNumber } </div>
                    <div class="small-desc"><strong>Current: </strong> <c:choose><c:when test="${cohort.isCurrent()}">Current</c:when><c:otherwise>Past</c:otherwise> </c:choose> </div>
                </div>
                <div class="small-bottom">
                   <c:url var="deleteHref" value="/users/${currentUser}/deleteCohort"/>
                    <br/><form action="${deleteHref}" method="POST"><input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" /><input id="deleteField" type="hidden" name="delete" value="true"><input type="hidden" name="userId" value="${employer.userId}"><input type="hidden" name="employerId" value="${employer.employerId}"><button class="btn btn-danger" type="submit" id="delete">Delete</button></form>
                </div>
            </div>
        </div>
    </c:forEach>
    </div>

<c:url var="blurHref" value="/js/stackBlur.js" />
<script src="${blurHref}"></script>

<c:import url="/WEB-INF/jsp/footer.jsp" />
		