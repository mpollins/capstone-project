<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:url var="formAction" value="/users/${userName}/newTechSkill" />
<form method="POST" action="${formAction}" id="createTechSKillForm">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<div class="row">
		<div class="col-sm-6">	
		
			<div class="form-group">
				<label for="location">Tech Skill Name: </label>
				<input type="text" id="techSkill" name="techSkill" maxlength="25" placeHolder="Technical Skill" class="form-control" />	
			</div>
			
			<button type="submit" class="btn btn-default">Save Tech Skill</button>
		</div>
	</div>
</form>
<br>
<c:url var="formAction" value="/users/${userName}/deleteTechSkill" />
<form method="POST" action="${formAction}" id="deleteTechSkillForm">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<div class="row">
		<div class="col-sm-6">	
		
			<div class="form-group">
			 <label for="techSkill">Technical Skills: </label>
			 <select class="selectpicker form-control" name="techSkill" data-width="auto"  data-size="6">
		 	  <c:forEach  var="techSkill" items="${ techSkills }">
		 	 	<option value="${ techSkill }">${ techSkill }</option>
			  </c:forEach> 
			 </select>
			</div>	
			
			<button type="submit" class="btn btn-default">Delete Tech Skill</button>
		</div>
	</div>
</form>
		
<c:url var="jsHref" value="/js/validateNewCohort.js"/>
<script src="${jsHref}"></script>
<c:import url="/WEB-INF/jsp/footer.jsp" />