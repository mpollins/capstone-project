<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var="year" value="${now}" pattern="yyyy" />

<c:import url="/WEB-INF/jsp/header.jsp" />


<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>
<!-- HEADER -->
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4"><h1>Edit Your Experience</h1></div>
	<div class="col-sm-4"></div>
</div>
<c:set var="listLength" value="${student.experiences.size()}"/>
<!-- Button dynamically generated with all education entries -->
<div class="row">
  <div class="col-sm-4"></div>
  <div class="col-sm-4 text-center">
  <div class="form-group">
  	<select class="selectpicker form-control" id="experienceButton" data-width="auto">
	  <option value="New" class="experienceSelect">New Experience</option>
	  <c:if test="${listLength gt 0}">
	  <c:forEach  var="experienceName" begin="0" end="${listLength - 1}">
	  	<option value="${experienceName}">${student.experiences.get(experienceName).company} - ${student.experiences.get(experienceName).title}</option>
	  </c:forEach> 
	  </c:if>
	</select>
	</div>
  </div>
  <div class="col-sm-4"></div>
</div>

<br/>
<c:url var="formAction" value="/users/${userName}/editExperience" />
<!-- Forms for each experience entry dynamically generated -->
<c:if test="${listLength gt 0}">
<c:forEach var="experienceVar" begin="0" end="${listLength - 1}">

<div class="row form-container" id="form${experienceVar}">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
  
  <form method="POST" action="${formAction}" id="experienceForm${experienceVar}">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
    <input type="hidden" name="studentId" value="${student.studentId}" />
    <input type="hidden" name="userId" value="${student.userId}" />
	<input type="hidden" name="experienceId" value="${student.experiences.get(experienceVar).experienceId}"/>
	<input id="deleteField" type="hidden" name="delete" value="false">
	<!-- Input fields -->
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="company" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Company</button></label>
      </span>
      <input type="text" class="form-control" value="${student.experiences.get(experienceVar).company}" name="company" maxlength="50" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="title" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Title</button></label>
      </span>
      <input type="text" class="form-control" value="${student.experiences.get(experienceVar).title}" name="title" maxlength="50" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="description" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Description</button></label>
      </span>
      <input type="text" class="form-control" value="${student.experiences.get(experienceVar).description}" name="description" maxlength="2000" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="startMonth" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Start Month</button></label>
      </span>
      <select class="selectpicker show-tick" id="startMonth" name="startMonth" data-width="100%">
      <option value="" title="Choose a month"></option>
      <c:forEach var="month" items="${months}">
      	<c:choose>
      		<c:when test="${student.experiences.get(experienceVar).startMonth == month}">
      			<option selected="selected" value="${month}">${month}</option>
     		</c:when>
     		<c:otherwise>
     			<option value="${month}">${month}</option>
     		</c:otherwise>
      	</c:choose>
      </c:forEach>
		</select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="startYear" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Start Year</button></label>
      </span>
      <select class="selectpicker show-tick" id="startYear" name="startYear" data-width="100%">
      	<option value="" title="Choose a year"></option>
      	<c:forEach  var="startingYear" begin="1960" end="${year}">
      		<fmt:parseNumber var="i" type="number" value="${student.experiences.get(experienceVar).startYear}" />
      		<c:choose>
      		<c:when test="${i == startingYear}">
      			<option selected="selected" value="${startingYear}">${startingYear}</option>
     		</c:when>
     		<c:otherwise>
     			<option value="${startingYear}">${startingYear}</option>
     		</c:otherwise>
      	</c:choose>
      	</c:forEach>
      </select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="endMonth" class="col-sm-2 control-label"><button class="btn btn-default" type="button">End Month</button></label>
      </span>
      <select class="selectpicker show-tick" id="endMonth" name="endMonth" data-width="100%">
      	<option value="" title="Choose a month"></option>
		  <c:forEach var="month" items="${months}">
      	<c:choose>
      		<c:when test="${student.experiences.get(experienceVar).endMonth == month}">
      			<option selected="selected" value="${month}">${month}</option>
     		</c:when>
     		<c:otherwise>
     			<option value="${month}">${month}</option>
     		</c:otherwise>
      	</c:choose>
      </c:forEach>
		</select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="endYear" class="col-sm-2 control-label"><button class="btn btn-default" type="button">End Year</button></label>
      </span>
      <select class="selectpicker show-tick" id="startYear" name="startYear" data-width="100%">
      <option value="" title="Choose a year"></option>
      	<c:forEach  var="endingYear" begin="1960" end="${year}">
      	<fmt:parseNumber var="j" type="number" value="${student.experiences.get(experienceVar).startYear}" />
      		<c:choose>
      		<c:when test="${j == endingYear}">
      			<option selected="selected" value="${endingYear}">${endingYear}</option>
     		</c:when>
     		<c:otherwise>
     			<option value="${endingYear}">${endingYear}</option>
     		</c:otherwise>
      	</c:choose>
      	</c:forEach>
      </select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    
    <!-- Submit Button -->
    <div class="col-sm-3"></div>
      <div class="col-sm-7 center-block">
        <button type="submit" class="btn btn-default" id="save">Save Changes</button>
        <button class="btn btn-danger" type="submit" id="delete">Delete School</button>
      </div>
    <div class="col-sm-2"></div>
</form>
</div>
<div class="col-sm-2"></div>
</div>
</c:forEach>
</c:if>
      
<!-- New form with blank fields and nothing set to read only -->

<div class="row form-container" id="formNew">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
  
  <form method="POST" action="${formAction}" id="newExperienceForm">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
    <input type="hidden" name="studentId" value="${student.studentId}" />
    <input type="hidden" name="userId" value="${user.userId}" />
    
	<!-- Input fields -->
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="company" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Company</button></label>
      </span>
      <input type="text" class="form-control" name="company">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="title" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Title</button></label>
      </span>
      <input type="text" class="form-control" name="title">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="description" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Description</button></label>
      </span>
      <input type="text" class="form-control" name="description">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="startMonth" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Start Month</button></label>
      </span>
      <select class="selectpicker show-tick" id="startMonth" name="startMonth" data-width="100%">
      <option value="" title="Choose a month"></option>
      <c:forEach var="month" items="${months }">
    	<option value="${month}">${month}</option>
      </c:forEach>
		</select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="startYear" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Start Year</button></label>
      </span>
      <select class="selectpicker show-tick" id="startYear" name="startYear" data-width="100%">
      	<option value="" title="Choose a year"></option>
      	<c:forEach  var="startingYear" begin="1960" end="${year}">
     		<option value="${startingYear}">${startingYear}</option>
      	</c:forEach>
      </select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="endMonth" class="col-sm-2 control-label"><button class="btn btn-default" type="button">End Month</button></label>
      </span>
      <select class="selectpicker show-tick" id="endMonth" name="endMonth" data-width="100%">
      	<option value="" title="Choose a month"></option>
		<c:forEach var="month" items="${months}">
     		<option value="${month}">${month}</option>
      	</c:forEach>
		</select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="endYear" class="col-sm-2 control-label"><button class="btn btn-default" type="button">End Year</button></label>
      </span>
      <select class="selectpicker show-tick" id="endYear" name="endYear" data-width="100%">
      	<option value="" title="Choose a year"></option>
      	<c:forEach  var="endingYear" begin="1960" end="${year}">
     		<option value="${endingYear}">${endingYear}</option>
      	</c:forEach>
      </select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    
    <!-- Submit Button -->
    <div class="col-sm-5"></div>
      <div class="col-sm-2 center-block">
        <button type="submit" class="btn btn-default" id="save">Save Changes</button>
      </div>
    <div class="col-sm-5"></div>
</form>
</div>
<div class="col-sm-2"></div>
</div>

<c:url var="jsHref" value="/js/validateExperience.js"/>  
<script src="${jsHref}"></script>         
<c:import url="/WEB-INF/jsp/footer.jsp" />