<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/WEB-INF/jsp/header.jsp" />
<div class="row">
	<div class="col-sm-3"></div>
	<div class="col-sm-6 text-center">
		<h2>All Users</h2>
	</div>
	<div class="col-sm-3"></div>
</div>

<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4 text-center">
		 <div class="form-group">
		 	<label for="cohortButton">User Type: </label>
		  	<select class="selectpicker form-control" id="cohortButton" data-width="auto">
			  <option value="permission-0">Staff</option>
			  <option value="permission-1">Student</option>
			  <option value="permission-2">Employer</option>
			</select>
		</div>
	</div>
	<div class="col-sm-4"></div>
</div>

    <div class="row">
    <c:forEach var="user" items="${allUsers}">
        	<div class="col-lg-3 col-sm-6 cohort-box permission-${user.permission}">

            <div class="small-card small-hovercard">
                <div class="small-cardheader card-background">
                	<c:url var="avatar" value="/img/avatar.png"/>
					<c:url var="backgroundImgHref" value="/img/${fn:toLowerCase(student.lastName)}${student.studentId}.jpg" />
            		<img class="card-bkimg" alt="" src="${backgroundImgHref}" onerror="this.src='${avatar}'">
                </div>
                <div class="small-avatar">
                	<c:set var="lowerCaseName" value="${student.lastName}" />
                    <c:url var="avatarHref" value="/img/${fn:toLowerCase(lowerCaseName)}${student.studentId}.jpg" />
                    <img src="${avatarHref}" alt="avatar" onerror="this.src='${avatar}'"/>
                </div>
                <div class="small-info">
                    <div class="small-title">
                        <p>${user.userName}</p>
                    </div>
                    <div class="small-desc"><strong>Permission Type: </strong><c:choose><c:when test="${user.permission eq 0}">Staff</c:when><c:when test="${user.permission eq 1}">Student</c:when><c:otherwise>Employer</c:otherwise></c:choose> </div>
                    <div class="small-desc"><strong>Account Status: </strong><c:choose><c:when test="${user.accountStatus}">Active</c:when><c:otherwise>Inactive</c:otherwise> </c:choose> </div>
                </div>
                <div class="small-bottom">
                   <c:url var="userHref" value="/users/${currentUser}/userProfile"/>
                    <form action="${userHref}" method="GET"><input type="hidden" name="userId" value="${user.userId}"><button type="submit" class="btn btn-default">User Profile</button></form>
                </div>
            </div>
        </div>
    </c:forEach>
    </div>

<c:url var="blurHref" value="/js/stackBlur.js" />
<script src="${blurHref}"></script>

<c:import url="/WEB-INF/jsp/footer.jsp" />
		