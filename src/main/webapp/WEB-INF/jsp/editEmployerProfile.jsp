<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>

<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<h1>Edit Your Profile</h1>
	</div>
	<div class="col-sm-4"></div>
</div>

<c:url var="formAction" value="/users/${userName}/editProfile" />
<form method="POST" action="${formAction}" id="newUserForm">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<input type="hidden" name="employerId" value="${employer.employerId}" />
	<input type="hidden" name="userId" value="${employer.userId}" />
	
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6" id="profile-div">
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="businessName" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Business Name</button></label>
		      </span>
		      <input type="text" class="form-control" value="${employer.businessName}" name="businessName" maxlength="50" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
		    </div>
		    <br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="firstName" class="col-sm-2 control-label"><button class="btn btn-default" type="button">First Name</button></label>
		      </span>
		      <input type="text" class="form-control" value="${employer.firstName}" name="firstName" maxlength="25" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
			<br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="lastName" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Last Name</button></label>
		      </span>
		      <input type="text" class="form-control" value="${employer.lastName}" name="lastName" maxlength="25" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
			<br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="description" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Description</button></label>
		      </span>
		      <textarea class="form-control" name="description" maxlength="2000" rows="3" readonly="readonly">${employer.description}</textarea>
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
			<br/>
			<br/>
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<button type="submit" class="btn btn-default">Save Changes</button>
			</div>
			<div class="col-sm-4"></div>
		</div>
		<div class="col-sm-3">
			
		</div>
	</div>
</form>

<c:url var="jsHref" value="/js/validateEmployerProfile.js"/>
<script src="${jsHref}"></script>
<c:import url="/WEB-INF/jsp/footer.jsp" />