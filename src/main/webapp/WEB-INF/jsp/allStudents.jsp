<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/WEB-INF/jsp/header.jsp" />
<div class="row">
	<div class="col-sm-3"></div>
	<div class="col-sm-6 text-center">
		<h2>All Students</h2>
	</div>
	<div class="col-sm-3"></div>
</div>

<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<div class="form-group">
		 	<select class="selectpicker form-control" id="cohortButton" data-width="auto">
			  <c:forEach  var="eachCohort" items="${cohorts}">
				  	<option value="cohort-${eachCohort.cohortId}">${eachCohort.location} Cohort ${eachCohort.programmingLang} [${eachCohort.cohortNumber}] <c:choose><c:when test="${eachCohort.isCurrent()}">- Current</c:when><c:otherwise>- Past</c:otherwise> </c:choose> </option>
				  </c:forEach> 
			</select>
		</div>
	</div>
	<div class="col-sm-4"></div>
</div>

    <div class="row">
    <c:forEach var="student" items="${allStudents}">
        	<div class="col-lg-3 col-sm-6 cohort-box cohort-${student.cohort.cohortId}">

            <div class="small-card small-hovercard">
                <div class="small-cardheader card-background">
                	<c:url var="avatar" value="/img/avatar.png"/>
					<c:url var="backgroundImgHref" value="/img/${fn:toLowerCase(student.lastName)}${student.studentId}.jpg" />
            		<img class="card-bkimg" alt="" src="${backgroundImgHref}" onerror="this.src='${avatar}'">
                </div>
                <div class="small-avatar">
                	<c:set var="lowerCaseName" value="${student.lastName}" />
                    <c:url var="avatarHref" value="/img/${fn:toLowerCase(lowerCaseName)}${student.studentId}.jpg" />
                    <img src="${avatarHref}" alt="avatar" onerror="this.src='${avatar}'"/>
                </div>
                <div class="small-info">
                    <div class="small-title">
                        <p>${student.firstName} ${student.lastName}</p>
                    </div>
                    <div class="small-desc"><strong>Cohort: </strong> ${student.cohort.location} - ${student.cohort.programmingLang} [${student.cohort.cohortNumber}]</div>
                    <div class="small-desc"><strong>Status: </strong><c:choose><c:when test="${student.cohort.isCurrent()}">Current Student</c:when><c:otherwise>Past Student</c:otherwise> </c:choose> </div>
                </div>
                <div class="small-bottom">
                   <c:url var="userHref" value="/users/${currentUser}/studentProfile"/>
                   <c:url var="deleteHref" value="/users/${currentUser}/deleteStudent"/>
                    <form action="${userHref}" method="GET"><input type="hidden" name="userId" value="${student.userId}"><input type="hidden" name="studentId" value="${student.studentId}"><button type="submit" class="btn btn-default">Profile</button></form><br/><form action="${deleteHref}" method="POST"><input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" /><input id="deleteField" type="hidden" name="delete" value="true"><input type="hidden" name="userId" value="${student.userId}"><input type="hidden" name="studentId" value="${student.studentId}"><button class="btn btn-danger" type="submit" id="delete">Delete</button></form>
                </div>
            </div>
        </div>
    </c:forEach>
    </div>

<c:url var="blurHref" value="/js/stackBlur.js" />
<script src="${blurHref}"></script>

<c:import url="/WEB-INF/jsp/footer.jsp" />
		