<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var="year" value="${now}" pattern="yyyy" />

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:set var="listLength" value="${ cohorts.size() - 1 }"/>
<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>
<!-- HEADER -->
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4"><h1>Edit Cohort</h1></div>
	<div class="col-sm-4"></div>
</div>

<!-- Button dynamically generated with all cohort entries -->
<div class="row">
  <div class="col-sm-4"></div>
  <div class="col-sm-4 text-center">
  <div class="form-group">
  	<select class="selectpicker form-control" data-width="auto">
	  <option value="New" class="cohortSelect">New Cohort</option>
	  <c:forEach  var="cohort" items="cohorts">
	  	<option value="${ cohort.cohortId }">{ cohort.location } Cohort ${ cohort.programmingLang } [${ cohort.cohortNumber }]</option>
	  </c:forEach> 
	</select>
	</div>
  </div>
  <div class="col-sm-4"></div>
</div>
<br/>
<c:url var="formAction" value="/users/${userName}/editCohort" />
<!-- Forms for each cohort entry dynamically generated -->
<c:forEach  var="cohortVar" begin="0" end="${listLength}">

<div class="row form-container" id="form${cohortVar}">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
  
  <form method="POST" action="${formAction}" id="schoolForm${cohortVar}">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<input type="hidden" name="cohortId" value="${ cohorts.get(cohortVar).cohortId }"/>
	<input id="deleteField" type="hidden" name="delete" value="false">
	<!-- Input fields -->
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="location" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Location</button></label>
      </span>
      <input type="text" class="form-control" value="${ cohorts.get(cohortVar).location }" name="location" maxlength="50" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="degree" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Degree</button></label>
      </span>
      <input type="text" class="form-control" value="${student.education.get(schoolVar).degree}" name="degree" maxlength="50" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="fieldOfStudy" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Field Of Study</button></label>
      </span>
      <input type="text" class="form-control" value="${student.education.get(schoolVar).fieldOfStudy}" name="fieldOfStudy" maxlength="50" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="description" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Description</button></label>
      </span>
      <input type="text" class="form-control" value="${student.education.get(schoolVar).description}" name="description" maxlength="2000" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    
    <!-- Submit Button -->
    <div class="col-sm-3"></div>
      <div class="col-sm-7 center-block">
        <button type="submit" class="btn btn-default" id="save">Save Changes</button>
        <button class="btn btn-danger" type="submit" id="delete">Delete Cohort</button>
      </div>
    <div class="col-sm-2"></div>
</form>
</div>
<div class="col-sm-2"></div>
</div>
</c:forEach>

<c:url var="jsHref" value="/js/validateEducation.js"/>  
<script src="${jsHref}"></script>   
<c:import url="/WEB-INF/jsp/footer.jsp" />