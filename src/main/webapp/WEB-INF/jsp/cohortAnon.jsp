<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/header.jsp" />

<h2>Current ${city} Cohort</h2>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<c:forEach items="${cohort}" var="student">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading${student.firstName}">
		      <h4 class="panel-title">
		        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse${student.firstName}" aria-expanded="false" aria-controls="collapse${student.firstName}">
		          ${student.firstName}
		        </a>
		      </h4>
		    </div>
	    <div id="collapse${student.firstName}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading${student.firstName}">
	      <div class="panel-body">
	        ${student.summary}
	      </div>
	    </div>
	  </div>
	</c:forEach>
</div>
		
<c:import url="/WEB-INF/jsp/footer.jsp" />
		