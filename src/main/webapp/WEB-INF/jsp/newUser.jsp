<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>

<c:url var="formAction" value="/users/${userName}/newUser" />
<form method="POST" action="${formAction}" id="newUserForm">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<div class="row">
		<div class="col-sm-6">	
		
			<div class="form-group">
				<label for="email">Email: </label>
				<input type="email" id="newUserEmail" name="email" placeHolder="Email Address" class="form-control" />	
			</div>
			<div class="form-group">
				<label for="firstName">First Name: </label>
				<input type="text" id="firstName" name="firstName" maxlength="25" placeHolder="First Name" class="form-control" />	
			</div>
			<div class="form-group">
				<label for="lastName">Last Name: </label>
				<input type="text" id="lastName" name="lastName" maxlength="25" placeHolder="Last Name" class="form-control" />	
			</div>
			<div class="form-group">
				<label for="userName">Username: </label>
				<input type="text" id="userName" name="userName" maxlength="25" placeHolder="Username" class="form-control" />
			</div>
			<div class="form-group">
				<label for="password">Password: </label>
				<input type="password" id="password" name="password" maxlength="128" placeHolder="Password" class="form-control" value="${password}" readonly="readonly"/>
			</div>
			<div class="form-group">
				<label for="confirmPassword">Confirm Password: </label>
				<input type="password" id="confirmPassword" name="confirmPassword" maxlength="128" class="form-control" value="${password}" readonly="readonly"/>	
			</div>
			<div class="form-group">
				<label for="permission">User Type: </label>
				<select name="permission" id="permission">
					<option value="0">Staff</option>
					<option value="1">Student</option>
					<option value="2">Employer</option>
				</select>
			</div>
			
			<div class="form-group student-form">
				<label for="cohortId">Cohort: </label>
				<select name="cohortId" id="cohortId">
					<c:forEach  var="cohortVar" items="${cohortList}">
						<option value="${cohortVar.cohortId}">${cohortVar.location} Cohort ${cohortVar.programmingLang} [${cohortVar.cohortNumber}]</option>
					</c:forEach>
				</select>
			</div>
			
			<div class="form-group employer-form">
				<label for="businessName">Business Name: </label>
				<input type="text" id="businessName" name="businessName" placeHolder="Business Name" class="form-control" />	
			</div>
			
			<button type="submit" class="btn btn-default">Create User</button>
		</div>
	</div>
</form>
		
<c:url var="jsHref" value="/js/validateNewUser.js"/>
<script src="${jsHref}"></script>
<c:import url="/WEB-INF/jsp/footer.jsp" />