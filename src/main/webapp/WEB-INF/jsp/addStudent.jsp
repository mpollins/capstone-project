<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/header.jsp" />

<h1>Add A New Student:</h1>

<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<c:url var="formAction" value="/addStudent" />
		<form method="POST" action="${formAction}">
			<div class="form-group">
				<label for="cohort">Cohort: </label>
				<select>
					<option value="Columbus Cohort [0]">Columbus Cohort [0]</option>
					<option value="Columbus Cohort [0]">Cleveland Cohort [0]</option>
					<option value="Columbus Cohort [0]">Cleveland Cohort [1]</option>
					<option value="Columbus Cohort [0]">Cleveland Cohort [2]</option>
					<option value="Columbus Cohort [0]">Cleveland Cohort [3]</option>
				</select>
			</div>
			<div class="form-group">
				<label for="firstName">First Name: </label>
				<input type="text" id="firstName" name="firstName" placeHolder="First Name" class="form-control" />
			</div>
			<div class="form-group">
				<label for="lastName">Last Name: </label>
				<input type="text" id="lastName" name="lastName" placeHolder="Last Name" class="form-control" />
			</div>
			<div class="form-group">
				<label for="summary">Student Summary: </label>
				<input type="text" id="summary" name="summary" placeHolder="Summary" class="form-control" />
			</div>
			<button type="submit" class="btn btn-default">Login</button>
		</form>
	</div>
	<div class="col-sm-4"></div>
</div>
<c:import url="/WEB-INF/jsp/footer.jsp" />