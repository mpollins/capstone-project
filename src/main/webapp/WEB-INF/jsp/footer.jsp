<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		</div>
		<div id="spacing"></div>
		<footer class="text-center navbar-fixed-bottom" id="footer">
		 <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <p class="copyright text-muted" id="footer-text"></p>
                </div>
            </div>
        </div>
		</footer>
		<c:url var="ourJsHref" value="/js/script.js" />
	    <script src="${ourJsHref}"></script>
	</body>
</html>