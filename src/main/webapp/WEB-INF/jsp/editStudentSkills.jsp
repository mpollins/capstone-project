<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var="year" value="${now}" pattern="yyyy" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:set var="listLength" value="${student.education.size() - 1}"/>
<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>
<!-- HEADER -->
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4"><h1>Edit Your Skills</h1></div>
	<div class="col-sm-4"></div>
</div>
<br/>


<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="jumbotron">
                <!-- Default panel contents -->
                <div class="page-header text-center"><h1>Tech Skills</h1></div>
            
                <!-- List group // ID and For must match for the button to work properly-->
                <ul class="list-group">
                <c:set var="indexCount" value="0" scope="page"/>
                <c:set var="allTechs" value="${studentTech}"/>
                <c:set var="allSofts" value="${studentSoft}"/>
                <c:forEach var="techSkill" items="${techSkillsList}">
                	<li class="list-group-item">
                        ${techSkill}
                        <div class="material-switch pull-right">
                        <c:set var="techSkillsPlus" value="${techSkill}"/>
                        <c:set var="contains" value="false" />
                        <c:forEach var="studentItem" items="${studentTech}">
                        	<c:if test="${ techSkill eq studentItem }">
                        		<c:set var="contains" value="true" />
                        	</c:if>
                        </c:forEach>
                        <c:choose>
						    <c:when test="${ contains }">
						       <input class="techSkill" id="techSkill${indexCount}" name="${techSkill}" type="checkbox" checked="checked"/>
						    </c:when>
						    <c:otherwise>
						        <input class="techSkill" id="techSkill${indexCount}" name="${techSkill}" type="checkbox"/>
						    </c:otherwise>
						</c:choose>
                        
                            <label for="techSkill${indexCount}" class="label-default"></label>
                        </div>
                    </li>
                    <c:set var="indexCount" value="${indexCount + 1}" scope="page"/>
                </c:forEach>
                </ul>
            </div> 
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="jumbotron">
                <!-- Default panel contents -->
                <div class="page-header text-center"><h1>Soft Skills</h1></div>
            
                <!-- List group // ID and For must match for the button to work properly-->
                <ul class="list-group">
                <c:set var="indexCount" value="0" scope="page"/>
                <c:forEach var="softSkill" items="${softSkillsList}">
                	<li class="list-group-item">
                        ${softSkill}
                        <div class="material-switch pull-right">
                        	<c:set var="contains" value="false" />
	                        <c:forEach var="studentItem" items="${studentSoft}">
	                        	<c:if test="${ softSkill eq studentItem }">
	                        		<c:set var="contains" value="true" />
	                        	</c:if>
	                        </c:forEach>
                        	<c:choose>
						    <c:when test="${ contains }">
						       <input class="softSkill" id="softSkill${indexCount}" name="${softSkill}" type="checkbox" checked="checked"/>
						    </c:when>
						    <c:otherwise>
						        <input class="softSkill" id="softSkill${indexCount}" name="${softSkill}" type="checkbox"/>
						    </c:otherwise>
						</c:choose>
                        	
                            <label for="softSkill${indexCount}" class="label-default"></label>
                        </div>
                    </li>
                    <c:set var="indexCount" value="${indexCount + 1}" scope="page"/>
                </c:forEach>
                </ul>
            </div> 
        </div>
    </div>
    
<div class="row">
	<div class="col-sm-offset-5 col-sm-2 center-block">
            	<c:url var="formAction" value="/users/${userName}/editSkills" />
            	<form method="POST" action="${formAction}" id="experienceForm">
            	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
    <input type="hidden" name="studentId" value="${student.studentId}" />
    <input type="hidden" name="userId" value="${student.userId}" />
            	<div class="hiddenSelectTech">
            		<select name="allTechSkills" class="selectpicker show-tick" id="selectTechSkills" multiple data-selected-text-format="count">
            		</select>
            	</div>
            	<div class="hiddenSelectSoft">
            		<select name="allSoftSkills" class="selectpicker show-tick" id="selectSoftSkills" multiple data-selected-text-format="count">
            		</select>
            	</div>
		        	<button type="submit" class="btn btn-default" id="saveStudentSkills">Save Changes</button>
		        </form>
		    </div>
</div>
    
        
<c:import url="/WEB-INF/jsp/footer.jsp" />