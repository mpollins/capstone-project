<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>

<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<h1>Edit Your Profile</h1>
	</div>
	<div class="col-sm-4"></div>
</div>

<c:url var="formAction" value="/users/${userName}/editProfile" />
<form method="POST" action="${formAction}" id="newUserForm">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<input type="hidden" name="studentId" value="${student.studentId}" />
	<input type="hidden" name="userId" value="${student.userId}" />
	<input type="hidden" name="contactId" value="${student.contact.contactId}" />
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6" id="profile-div">
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="firstName" class="col-sm-2 control-label"><button class="btn btn-default" type="button">First Name</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.firstName}" name="firstName" maxlength="25" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
		    </div>
		    <br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="lastname" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Last Name</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.lastName}" name="lastName" maxlength="25" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
			<br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="email" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Email</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.contact.email}" name="email" maxlength="50" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
			<br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="summary" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Summary</button></label>
		      </span>
		      <textarea class="form-control" name="summary" maxlength="500" rows="3" readonly="readonly">${student.summary}</textarea>
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
			<br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="interests" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Interests</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.interests}" name="interests" maxlength="250" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
		    <br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="address1" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Address 1</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.contact.address1}" name="address1" maxlength="50" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
		    <br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="address2" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Address 2</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.contact.address2}" name="address2" maxlength="50" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
		    <br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="city" class="col-sm-2 control-label"><button class="btn btn-default" type="button">City</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.contact.city}" name="city" maxlength="25" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
		    <br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="state" class="col-sm-2 control-label"><button class="btn btn-default" type="button">State</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.contact.state}" name="state" maxlength="15" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
		    <br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="zip" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Zip</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.contact.zip}" name="zip" maxlength="15" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
		     <br/>
			<div class="input-group">
		      <span class="input-group-btn edit">
		        <label for="phone" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Phone</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.contact.phone}" name="phone" maxlength="25" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
		    <br/>
			<div class="input-group text-center">
		      <span class="input-group-btn discoverable-toggle">
		        <label for="discoverable" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Discoverable</button></label>
		      </span>
		      <input type="text" class="form-control" value="${student.discoverable}" name="discoverable" readonly="readonly">
		      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback" aria-hidden="true"></span>
		    </div>
			<br/>
			<br/>
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<button type="submit" class="btn btn-default">Save Changes</button>
			</div>
			<div class="col-sm-4"></div>
		</div>
		<div class="col-sm-3">
			
		</div>
	</div>
</form>

<c:url var="jsHref" value="/js/validateStudentProfile.js"/>
<script src="${jsHref}"></script>
<c:import url="/WEB-INF/jsp/footer.jsp" />