<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/header.jsp" />
<div class="row">
	<div class="col-sm-3"></div>
	<div class="col-sm-6 text-center">
		<h2>All Employers</h2>
	</div>
	<div class="col-sm-3"></div>
</div>

    <div class="row">
    <c:forEach var="employer" items="${allEmployers}">
        	<div class="col-lg-3 col-sm-6">

            <div class="small-card small-hovercard">
                <div class="small-cardheader">

                </div>
                <div class="small-avatar">
                    <c:url var="avatarHref" value="/img/avatar.png" />
                    <img src="${avatarHref}" alt="avatar"/>
                </div>
                <div class="small-info">
                    <div class="small-title">
                        <p>${employer.businessName}</p>
                    </div>
                    <div class="small-desc"><strong>Contact Name: </strong> ${employer.firstName} ${employer.lastName}</div>
                    <div class="small-desc"><strong>Description: </strong> ${employer.description}</div>
                </div>
                <div class="small-bottom">
                   <c:url var="employerHref" value="/users/${currentUser}/employerProfile"/>
                   <c:url var="deleteHref" value="/users/${currentUser}/deleteEmployer"/>
                    <form action="${employerHref}" method="GET"><input type="hidden" name="userId" value="${employer.userId}"><input type="hidden" name="employerId" value="${employer.employerId}"><button type="submit" class="btn btn-default">Profile</button><c:if test="${thisUser.permission eq 0}"></form><br/><form action="${deleteHref}" method="POST"><input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" /><input id="deleteField" type="hidden" name="delete" value="true"><input type="hidden" name="userId" value="${employer.userId}"><input type="hidden" name="employerId" value="${employer.employerId}"><button class="btn btn-danger" type="submit" id="delete">Delete</button></form></c:if> 
                </div>
            </div>
        </div>
    </c:forEach>
    </div>

<c:url var="blurHref" value="/js/stackBlur.js" />
<script src="${blurHref}"></script>

<c:import url="/WEB-INF/jsp/footer.jsp" />
		