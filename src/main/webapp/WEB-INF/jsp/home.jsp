<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<header class="intro-header" id="header-img"
	style="background-image: url('https://static1.squarespace.com/static/55ef2da9e4b03f6e1ef0cd28/t/579bb286e58c62582a241cf4/1470059580936/Columbus-OH-Skyline.jpg?format=1500w');">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<div class="site-heading">
					<h1>Tech Elevator</h1>
					<hr class="small">
					<br>
					<span class="subheading">Coding bootcamp for students who want
	more out of their careers<!-- Like a regular elevator but more
						techy --></span>
						<br>
						<br>
						<br>
				<span class="subheading"><a href="http://www.techelevator.com/apply" style="color:white;text-decoration:none">Apply Now</a></span> 
				</div>
			</div>
		</div>
	</div>
</header>

<c:import url="/WEB-INF/jsp/footer.jsp" />

