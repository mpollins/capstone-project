<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>

<div class="row">
	<div class="col-sm-3"></div>
	<div class="col-sm-6">
		<h1>Search Options</h1>
	</div>
	<div class="col-sm-3"></div>
</div>

<c:url var="formAction" value="/users/${currentUser}/searchStudents" />
<form method="POST" action="${formAction}" id="newUserForm">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6" id="profile-div">
			<!-- Programming Language Select -->
			<div class="form-group">
			 <label for="language">Programming Language: </label>
			 <select class="selectpicker form-control" name="language" data-width="auto">
		 	  <c:forEach  var="language" items="${ languages }">
		 	 	<option value="${ language }">${ language }</option>
			  </c:forEach> 
			 </select>
			</div>
		    <br/>
			<!-- Technical Skill Select -->
			<div class="form-group">
			 <label for="techSkills">Technical Skills: </label>
			 <select class="selectpicker form-control" name="techSkills" multiple data-width="auto">
		 	  <c:forEach  var="techSkill" items="${ techSkills }">
		 	 	<option value="${ techSkill }">${ techSkill }</option>
			  </c:forEach> 
			 </select>
			</div>			
			
			<br/>
			
			<div class="form-group">
			 <label for="degree">Education Level: </label>
			 <select class="selectpicker form-control" name="degree" data-width="auto">
		 	  	<option value="0">None</option>
		 	 	<option value="1">Certification</option>
		 	 	<option value="2">2 Year Degree</option>
		 	 	<option value="3">4 Year Degree</option>
		 	 	<option value="4">Higher Degree</option>
			 </select>
			</div>	
			
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<button type="submit" class="btn btn-default">Search</button>
			</div>
			<div class="col-sm-4"></div>
		</div>
		<div class="col-sm-3">
			
		</div>
	</div>
</form>

<c:url var="jsHref" value="/js/validateStudentProfile.js"/>
<script src="${jsHref}"></script>
<c:import url="/WEB-INF/jsp/footer.jsp" />