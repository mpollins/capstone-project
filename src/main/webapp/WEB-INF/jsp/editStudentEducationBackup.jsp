<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var="year" value="${now}" pattern="yyyy" />

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:set var="listLength" value="${student.education.size() - 1}"/>
<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>
<!-- HEADER -->
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4"><h1>Edit Your Education</h1></div>
	<div class="col-sm-4"></div>
</div>

<!-- Button dynamically generated with all education entries -->
<div class="row">
  <div class="col-sm-4"></div>
  <div class="col-sm-4 text-center">
  <div class="form-group">
  	<select class="selectpicker form-control" id="schoolButton" data-width="auto">
	  <option value="New" class="schoolSelect">New School</option>
	  <c:if test="${listLength gt 0}">
	  <c:forEach  var="schoolName" begin="0" end="${listLength}">
	  <option value="${schoolName}">${student.education.get(schoolName).school} - ${student.education.get(schoolName).degree}</option>
	  </c:forEach> 
	  </c:if>
	</select>
	</div>
  </div>
  <div class="col-sm-4"></div>
</div>
<br/>
<c:url var="formAction" value="/users/${userName}/editEducation" />
<!-- Forms for each education entry dynamically generated -->
<c:if test="${listLength gt 0}">
<c:forEach  var="schoolVar" begin="0" end="${listLength}">

<div class="row form-container" id="form${schoolVar}">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
  
  <form method="POST" action="${formAction}" id="schoolForm${schoolVar}">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
    <input type="hidden" name="studentId" value="${student.studentId}" />
    <input type="hidden" name="userId" value="${student.userId}" />
	<input type="hidden" name="educationId" value="${student.education.get(schoolVar).educationId }"/>
	<!-- Input fields -->
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="school" class="col-sm-2 control-label"><button class="btn btn-default" type="button">School</button></label>
      </span>
      <input type="text" class="form-control" value="${student.education.get(schoolVar).school}" name="school" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="degree" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Degree</button></label>
      </span>
      <input type="text" class="form-control" value="${student.education.get(schoolVar).degree}" name="degree" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="fieldOfStudy" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Field Of Study</button></label>
      </span>
      <input type="text" class="form-control" value="${student.education.get(schoolVar).fieldOfStudy}" name="fieldOfStudy" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="startMonth" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Start Month</button></label>
      </span>
      <select class="selectpicker show-tick" id="startMonth" name="startMonth" data-width="100%">
      <option value="" title="Choose a month"></option>
      <c:forEach var="month" items="${months}">
      	<c:choose>
      		<c:when test="${student.education.get(schoolVar).startMonth == month}">
      			<option selected="selected" value="${month}">${month}</option>
     		</c:when>
     		<c:otherwise>
     			<option value="${month}">${month}</option>
     		</c:otherwise>
      	</c:choose>
      </c:forEach>
		</select>
      </span>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="startYear" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Start Year</button></label>
      </span>
      <select class="selectpicker show-tick" id="startYear" name="startYear" data-width="100%">
      	<option value="" title="Choose a year"></option>
      	<c:forEach  var="startingYear" begin="1960" end="${year}">
      		<c:choose>
      		<c:when test="${student.education.get(schoolVar).startYear == startingYear}">
      			<option selected="selected" value="${startingYear}">${startingYear}</option>
     		</c:when>
     		<c:otherwise>
     			<option value="${startingYear}">${startingYear}</option>
     		</c:otherwise>
      	</c:choose>
      	</c:forEach>
      </select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="endMonth" class="col-sm-2 control-label"><button class="btn btn-default" type="button">End Month</button></label>
      </span>
      <select class="selectpicker show-tick" id="endMonth" name="endMonth" data-width="100%">
      	<option value="" title="Choose a month"></option>
		  <c:forEach var="month" items="${months}">
      	<c:choose>
      		<c:when test="${student.education.get(schoolVar).endMonth == month}">
      			<option selected="selected" value="${month}">${month}</option>
     		</c:when>
     		<c:otherwise>
     			<option value="${month}">${month}</option>
     		</c:otherwise>
      	</c:choose>
      </c:forEach>
		</select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="endYear" class="col-sm-2 control-label"><button class="btn btn-default" type="button">End Year</button></label>
      </span>
      <select class="selectpicker show-tick" id="endYear" name="endYear" data-width="100%">
      <option value="" title="Choose a year"></option>
      	<c:forEach  var="endingYear" begin="1960" end="${year}">
      	<c:choose>
      		<c:when test="${student.education.get(schoolVar).endYear == endingYear}">
      			<option selected="selected" value="${endingYear}">${endingYear}</option>
     		</c:when>
     		<c:otherwise>
     			<option value="${endingYear}">${endingYear}</option>
     		</c:otherwise>
      	</c:choose>
      	</c:forEach>
      </select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="description" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Description</button></label>
      </span>
      <input type="text" class="form-control" value="${student.education.get(schoolVar).description}" name="description" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    
    <!-- Submit Button -->
    <div class="col-sm-3"></div>
      <div class="col-sm-7 center-block">
        <button type="submit" class="btn btn-default" id="save">Save Changes</button>
        <button class="btn btn-danger" type="submit" id="delete">Delete School</button>
      </div>
    <div class="col-sm-2"></div>
</form>
</div>
<div class="col-sm-2"></div>
</div>
</c:forEach>
</c:if>
      
<div class="row form-container" id="formNew">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
  
  <form method="POST" action="${formAction}" id="newSchoolForm">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
    <input type="hidden" name="studentId" value="${student.studentId}" />
    <input type="hidden" name="userId" value="${student.userId}" />
	
	<!-- Input fields -->
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="school" class="col-sm-2 control-label"><button class="btn btn-default" type="button">School</button></label>
      </span>
      <input type="text" class="form-control" name="school">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="degree" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Degree</button></label>
      </span>
      <input type="text" class="form-control" name="degree">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="fieldOfStudy" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Field Of Study</button></label>
      </span>
      <input type="text" class="form-control" name="fieldOfStudy">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="startMonth" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Start Month</button></label>
      </span>
		<select class="selectpicker show-tick" id="startMonth" name="startMonth" data-width="100%">
	      <option value="" title="Choose a month"></option>
	      <c:forEach var="month" items="${months }">
	    	<option value="${month}">${month}</option>
	      </c:forEach>
		</select>      
		<span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="startYear" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Start Year</button></label>
      </span>
      <select class="selectpicker show-tick" id="startYear" name="startYear" data-width="100%">
      	<option value="" title="Choose a year"></option>
      	<c:forEach  var="startingYear" begin="1960" end="${year}">
     		<option value="${startingYear}">${startingYear}</option>
      	</c:forEach>
      </select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="endMonth" class="col-sm-2 control-label"><button class="btn btn-default" type="button">End Month</button></label>
      </span>
      <select class="selectpicker show-tick" id="endMonth" name="endMonth" data-width="100%">
      	<option value="" title="Choose a month"></option>
		<c:forEach var="month" items="${months}">
     		<option value="${month}">${month}</option>
      	</c:forEach>
		</select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="endYear" class="col-sm-2 control-label"><button class="btn btn-default" type="button">End Year</button></label>
      </span>
      <select class="selectpicker show-tick" id="endYear" name="endYear" data-width="100%">
      	<option value="" title="Choose a year"></option>
      	<c:forEach  var="endingYear" begin="1960" end="${year}">
     		<option value="${endingYear}">${endingYear}</option>
      	</c:forEach>
      </select>
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="description" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Description</button></label>
      </span>
      <input type="text" class="form-control" name="description">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    
    <!-- Submit Button -->
    <div class="col-sm-5"></div>
      <div class="col-sm-2 center-block">
        <button type="submit" class="btn btn-default" id="save">Save Changes</button>
      </div>
    <div class="col-sm-5"></div>
</form>
</div>
<div class="col-sm-2"></div>
</div>
        
<c:import url="/WEB-INF/jsp/footer.jsp" />