<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/header.jsp" />
<div class="row">
<div class="col-lg-2 col-sm-2"></div>
<div class="col-lg-8 col-sm-8">
    <div class="profile-card hovercard">
        <div class="card-background">
        	<c:url var="backgroundImgHref" value="/img/${staff.lastName}.png" />
            <img class="card-bkimg" alt="" src="${backgroundImgHref}">
        </div>
        <div class="useravatar">
        	<c:url var="userImage" value="/img/${staff.lastName}.png"/>
            <img alt="" src="${userImage}">
        </div>
        <div class="card-info"> <span class="card-title">${staff.firstName} ${staff.lastName}</span>

        </div>
    </div>
    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button type="button" id="stars" class="btn btn-primary" href="#" data-toggle="tab" disabled="disabled"><span class="fa fa-user" aria-hidden="true"></span>
                <div class="hidden-xs">Staff Info</div>
            </button>
        </div>
<!--         <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="fa fa-graduation-cap" aria-hidden="true"></span>
                <div class="hidden-xs">Education</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="fa fa-code" aria-hidden="true"></span>
                <div class="hidden-xs">Skills/Experience</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="following" class="btn btn-default" href="#tab4" data-toggle="tab"><span class="fa fa-folder-open" aria-hidden="true"></span>
                <div class="hidden-xs">Portfolio</div>
            </button>
        </div> -->
    </div>

      <div class="well">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
		 <form class="form-horizontal">
		  <div class="form-group">
		    <label class="col-sm-2 control-label"><span class="fa fa-envelope-open-o fa-2x" aria-hidden="true"></span></label>
		    <div class="col-sm-9">
		      <p class="form-control-static"><a href="mailto:#">${ staff.contact.email }</a></p>
		    </div>
		    <div class="col-sm-4"></div>
		  </div>
<%-- 		  <c:forEach var="media" items="${ student.socialMedia }">
		  <div class="form-group">
		    <label class="col-sm-2 control-label">
		    <c:choose>
		     <c:when test="${ media.name eq 'twitter' }"><span class="fa fa-twitter fa-2x" aria-hidden="true"></span></c:when>
		     <c:when test="${ media.name eq 'facebook' }"><span class="fa fa-facebook fa-2x" aria-hidden="true"></span></c:when>
		     <c:when test="${ media.name eq 'web' }"><span class="fa fa-link fa-2x" aria-hidden="true"></span></c:when>
		     <c:when test="${ media.name eq 'linkedIn' }"><span class="fa fa-linkedin fa-2x" aria-hidden="true"></span></c:when>
		     <c:otherwise><span class="fa fa-paperclip fa-2x" aria-hidden="true"></span></c:otherwise>
		    </c:choose>
		    </label>
		    <div class="col-sm-9">
		      <p class="form-control-static"><a href=${ media.link }>${ media.link }</a></p>
		    </div>
		    <div class="col-sm-4"></div>
		  </div>
		  </c:forEach> --%>
		  <div class="form-group">
		    <label class="col-sm-2 control-label"><span class="fa fa-mobile fa-3x" aria-hidden="true"></span></label>
		    <div class="col-sm-9">
		      <p class="form-control-static">${ staff.contact.phone }</p>
		    </div>
		    <div class="col-sm-4"></div>
		  </div>
		  </form>
        </div>
        
        <%-- <div class="tab-pane fade in" id="tab2">
          <c:forEach var="item" items="${ student.education }"> 
          <form class="form-horizontal">
		  <div class="form-group">
		    <label class="col-sm-2 control-label"><span class="fa fa-graduation-cap fa-2x" aria-hidden="true"></span></label>
		    <div class="col-sm-9">
		      <h4>${ item.school }</h4>
		      <p>${ item.degree } ${ item.fieldOfStudy }</p>
		      <p>${ item.startMonth } ${ item.startYear } - ${ item.endMonth } ${ item.endYear }</p>
		      <p>${ item.description }</p>
		    </div>
		    <div class="col-sm-4"></div>
		  </div>
		  </form>
		  </c:forEach>
        </div>
        
        <div class="tab-pane fade in" id="tab3">
        <form class="form-horizontal">
          <div class="form-group">
          <label class="col-sm-2 control-label"><span class="fa fa-code fa-2x" aria-hidden="true"></span></label>
          <div class="col-sm-9">
           <c:forEach var="tech" items="${ student.techSkills }">
           	| ${ tech }
           </c:forEach>
          |</div>
          </div>
          <div class="form-group">
          <label class="col-sm-2 control-label"><span class="fa fa-user-circle fa-2x" aria-hidden="true"></span></label>
          <div class="col-sm-9">
           <c:forEach var="soft" items="${ student.softSkills }">
           	| ${ soft }
           </c:forEach>
          |</div>
          </div>
          <c:forEach var="item" items="${ student.experiences }"> 
		  <div class="form-group">
		    <label class="col-sm-2 control-label"><span class="fa fa-briefcase fa-2x" aria-hidden="true"></span></label>
		    <div class="col-sm-9">
		      <h4>${ item.company }</h4>
		      <p>${ item.title }</p>
		      <p>${ item.startMonth } ${ item.startYear } - ${ item.endMonth } ${ item.endYear }</p>
		      <p>${ item.description }</p>
		    </div>
		    <div class="col-sm-4"></div>
		  </div>
		  </c:forEach>
	    </form>
		</div>
		
		<div class="tab-pane fade in" id="tab4">
          <c:forEach var="item" items="${ student.portfolio }"> 
          <form class="form-horizontal">
		  <div class="form-group">
		    <label class="col-sm-2 control-label"><span class="fa fa-folder-open-o fa-2x" aria-hidden="true"></span></label>
		    <div class="col-sm-9">
		      <h4>${ item.projectName }</h4>
		      <p>
		      <a href="${ item.link }">${ item.link }</a>
		      </p>
		      <p>${ item.description }</p>
		    </div>
		    <div class="col-sm-4"></div>
		  </div>
		  </form>
		  </c:forEach>
        </div> --%>
        
      </div>
    </div>
    
    </div>
    <div class="col-lg-2 col-sm-2"></div>
</div>

<c:import url="/WEB-INF/jsp/footer.jsp" />
		