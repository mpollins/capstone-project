<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Tech Elevator Student Profiles</title>
		<c:url var="bootstrapHref" value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="stylesheet" href="${bootstrapHref}">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<c:url var="jQueryMinHref" value="/js/jquery.min.js" />
	    <script src="${jQueryMinHref}"></script>
	    <c:url var="jQueryValidateHref" value="/js/jquery.validate.min.js" />
	    <script src="${jQueryValidateHref}"></script>
	    <c:url var="jQueryMethodsHref" value="/js/additional-methods.min.js" />
	    <script src="${jQueryMethodsHref}"></script>
	    <script src="https://cdn.jsdelivr.net/jquery.timeago/1.4.1/jquery.timeago.min.js"></script>
	    <c:url var="bootstrapJsHref" value="/js/bootstrap.min.js" />
	    <script src="${bootstrapJsHref}"></script>
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css">
	    <c:url var="cssHref" value="/css/site.css" />
		<link rel="stylesheet" type="text/css" href="${cssHref}">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js"></script>

		
		<script type="text/javascript">
			$(document).ready(function() {
				$("time.timeago").timeago();
				
				$("#logoutLink").click(function(event){
					$("#logoutForm").submit();
				});
				
				var pathname = window.location.pathname;
				$("nav a[href='"+pathname+"']").parent().addClass("active");
			});
		</script>
		
	</head>
	<body>
		<header><!-- Link Referencecs -->
			<c:url var="homePageHref" value="http://www.techelevator.com"/>
			<c:url var="homeLinkHref" value="/home"/>
			<c:url var="imgSrc" value="http://static1.squarespace.com/static/55ef2da9e4b03f6e1ef0cd28/t/57ae02eeebbd1aaf598d3b32/1480957490797/?format=1500w" />
			<c:url var="cohortClePublic" value="/cohortAnonCleveland" />
			<c:url var="cohortColPublic" value="/cohortAnonColumbus" />
			<c:url var="storyHref" value="http://www.techelevator.com/student-success/" />
			<c:url var="dashboardHref" value="/users/${currentUser}" />
			<c:url var="cohortCleHref" value="/users/${currentUser}/cohortUserViewCleveland" />
			<c:url var="cohortColHref" value="/users/${currentUser}/cohortUserViewColumbus" />
			<c:url var="personalDetailsHref" value="/users/${currentUser}/editProfile" />
			<c:url var="changePasswordHref" value="/users/${currentUser}/changePassword"/>
			<c:url var="eventsHref" value="https://techelevatorcbus_showcases.eventbrite.com/" />
			<c:url var="workHref" value="/users/${currentUser}/editExperience" />
			<c:url var="educationHref" value="/users/${currentUser}/editEducation" />
			<c:url var="skillsHref" value="/users/${currentUser}/editSkills" />
			<c:url var="socialMediaHref" value="/users/${currentUser}/editSocialMedia" />
			<c:url var="portfolioHref" value="/users/${currentUser}/editPortfolio" />
			<c:url var="personalProfileHref" value="/users/${currentUser}/studentProfile?studentId=${user.userId}" />
			<c:url var="techStackHref" value="/users/${currentUser}/editTechStack" />
			<c:url var="viewAllEmployersHref" value="/users/${currentUser}/viewAllEmployers" />
			<c:url var="searchStudentHref" value="/users/${currentUser}/searchStudents" /> 
		</header>
		<nav class="navbar navbar-default navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a href="${homePageHref}" class="navbar-brand hidden-xs"><img id="logo" src="${imgSrc}" alt="Brand"/></a>
			      <a href="${homePageHref}" class="navbar-brand visible-xs"><img id="logo" src="${imgSrc}" alt="Brand"/></a>
			    </div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			     <ul class="nav navbar-nav">
			     
			     	<c:choose>
			     		<c:when test="${user.permission eq 0}"> <!-- Permission 0 represents staff/admin accounts -->
			     			<!-- Link references for staff members -->
			     			<c:url var="viewAllUsersHref" value="/users/${currentUser}/viewAllUsers" />
					        <c:url var="viewAllStaffHref" value="/users/${currentUser}/viewAllStaff" />
					        <c:url var="viewAllStudentsHref" value="/users/${currentUser}/viewAllStudents" />
					        <c:url var="viewAllCohortsHref" value="/users/${currentUser}/viewAllCohorts" />
					        <c:url var="addNewUser" value="/users/${currentUser}/newUser" />
					        <c:url var="addNewCohort" value="/users/${currentUser}/newCohort" />
					        <c:url var="addNewTechSkill" value="/users/${currentUser}/newTechSkill" />
			     			<li><a href="${homeLinkHref}">Home</a></li>
							<li><a href="${dashboardHref}">Dashboard</a></li>
							<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cohorts <span class="caret"></span></a>
					          <ul class="dropdown-menu">
					            <li><a href="${cohortColHref}">Columbus</a></li>
					            <li><a href="${cohortCleHref}">Cleveland</a></li>
					          </ul>
					        </li>
					        <li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
					          <ul class="dropdown-menu">
					          	<li><a href="${addNewUser}">Add New User</a></li>
					          	<li><a href="${addNewCohort}">Add New Cohort</a></li>					          	<li><a href="${addNewTechSkill}">Add/Delete Tech Skills</a></li>
					            <li><a href="${viewAllUsersHref}">Users</a></li>
					            <li><a href="${viewAllStaffHref}">Staff</a></li>
					            <li><a href="${viewAllStudentsHref}">Students</a></li>
					            <li><a href="${viewAllEmployersHref}">Employers</a></li>
					            <li><a href="${viewAllCohortsHref}">Cohorts</a></li>
					          </ul>
					        </li>
							<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profile <span class="caret"></span></a>
					          <ul class="dropdown-menu">
					            <li><a href="${personalDetailsHref}">Personal Details</a></li>
					            <li><a href="${changePasswordHref}">Change Password</a></li>
					          </ul>
					        </li>
					        <li><a href="${searchStudentHref}">Search Students</a></li>
			     		</c:when>
			     		<c:when test="${user.permission eq 1}"> <!-- Permission level 1 represents students -->
							<li><a href="${homeLinkHref}">Home</a></li>
							<li><a href="${dashboardHref}">Dashboard</a></li>
							<li><a href="${viewAllEmployersHref}">Employers</a></li>
							<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cohorts <span class="caret"></span></a>
					          <ul class="dropdown-menu">
					            <li><a href="${cohortColHref}">Columbus</a></li>
					            <li><a href="${cohortCleHref}">Cleveland</a></li>
					          </ul>
					        </li>
							<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profile <span class="caret"></span></a>
					          <ul class="dropdown-menu">
					          	<li><a href="${personalProfileHref}">Your Profile</a></li>
					            <li><a href="${personalDetailsHref}">Personal Details</a></li>
					            <li><a href="${educationHref}">Education</a></li>
					            <li><a href="${workHref}">Work History</a></li>
					            <li><a href="${skillsHref}">Skills</a></li>
					            <li><a href="${socialMediaHref}">Social Media</a></li>
					            <li><a href="${portfolioHref}">Portfolio</a></li>
					            <li><a href="${changePasswordHref}">Change Password</a></li>
					          </ul>
					        </li>
			     		</c:when>
			     		<c:when test="${user.permission eq 2}"> <!-- Permission level 2 is for employers -->
			     			<li><a href="${homeLinkHref}">Home</a></li>
							<li><a href="${dashboardHref}">Dashboard</a></li>
							<li><a href="${eventsHref}">Event Signup</a></li>
							<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cohorts <span class="caret"></span></a>
					          <ul class="dropdown-menu">
					            <li><a href="${cohortColHref}">Columbus</a></li>
					            <li><a href="${cohortCleHref}">Cleveland</a></li>
					          </ul>
					        </li>
							<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profile <span class="caret"></span></a>
					          <ul class="dropdown-menu">
					            <li><a href="${personalDetailsHref}">Personal Details</a></li>
					            <!--<li><a href="${techStackHref}">Tech Stack</a></li>
					            <li><a href="${socialMediaHref}">Social Media</a></li>
					            <li><a href="${portfolioHref}">Portfolio</a></li>-->
					            <li><a href="${changePasswordHref}">Change Password</a></li>
					          </ul>
					        </li>
					        <li><a href="${searchStudentHref}">Search Students</a></li>
			     		</c:when>
			     		<c:otherwise>
						<!-- NavBar to be displayed for anonymous users -->
						<li><a href="${homeLinkHref}">Home</a></li>
						<li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cohorts <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="${cohortColPublic}">Columbus</a></li>
				            <li><a href="${cohortClePublic}">Cleveland</a></li>
				          </ul>
				        </li>
				        <li><a href="${storyHref}">Student Stories</a></li>
				        <li><a href="http://www.techelevator.com/contact">Contact Us</a></li>
			     		</c:otherwise>
			     	</c:choose>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<c:choose>
							<c:when test="${empty currentUser}">
								<c:url var="loginHref" value="/login" />
								<li><a href="${loginHref}">Log In</a></li>
							</c:when>
							<c:otherwise>
								<c:url var="logoutAction" value="/logout" />
								
								<form id="logoutForm" action="${logoutAction}" method="POST">
									<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
								</form>
								<li><a id="logoutLink" href="#">Log Out</a></li>
							</c:otherwise>
						</c:choose>
					</ul>
					
			    </div><!-- /.navbar-collapse -->
			</div>
		</nav>	
		<div class="container">