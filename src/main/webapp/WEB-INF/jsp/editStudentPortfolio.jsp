<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var="year" value="${now}" pattern="yyyy" />

<c:import url="/WEB-INF/jsp/header.jsp" />

<c:set var="listLength" value="${student.portfolio.size() - 1}"/>
<c:url var="pwValidationSrc" value="/js/passwordValidation.js" />
<script src="${pwValidationSrc}"></script>
<!-- HEADER -->
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4"><h1>Edit Your Portfolio</h1></div>
	<div class="col-sm-4"></div>
</div>

<!-- Button dynamically generated with all education entries -->
<div class="row">
  <div class="col-sm-4"></div>
  <div class="col-sm-4 text-center">
  <div class="form-group">
  	<select class="selectpicker form-control" id="portfolioButton" data-width="auto">
	  <option value="NewPortfolio" class="portfolioSelect">New Project</option>
	  <c:if test="${listLength gt 0}">
	  <c:forEach  var="project" begin="0" end="${listLength}">
	  <option value="${project}">${student.portfolio.get(project).projectName}</option>
	  </c:forEach> 
	  </c:if>
	</select>
	</div>
  </div>
  <div class="col-sm-4"></div>
</div>
<br/>
<c:url var="formAction" value="/users/${userName}/editPortfolio" />
<!-- Forms for each education entry dynamically generated -->
<c:if test="${listLength gt 0}">

<c:forEach  var="projectVar" begin="0" end="${listLength}">
<div class="row portfolio-form-container" id="form${projectVar}">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
  
  <form method="POST" action="${formAction}" id="portfolioForm${projectVar}">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
    <input type="hidden" name="studentId" value="${student.studentId}" />
    <input type="hidden" name="userId" value="${student.userId}" />
	<input type="hidden" name="portfolioId" value="${student.portfolio.get(projectVar).portfolioId}"/>
	<input id="deleteField" type="hidden" name="delete" value="false">

	<!-- Input fields -->
    <div class="input-group">
      <span class="input-group-btn">
        <label for="projectName" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Project</button></label>
      </span>
      <input type="text" class="form-control" value="${student.portfolio.get(projectVar).projectName}" name="projectName" maxlength="100" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="description" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Description</button></label>
      </span>
      <input type="text" class="form-control" value="${student.portfolio.get(projectVar).description}" name="description" maxlength="2000" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="link" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Link</button></label>
      </span>
      <input type="text" class="form-control" value="${student.portfolio.get(projectVar).link}" name="link" maxlength="100" readonly="readonly">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <!-- Submit Button -->
    <div class="col-sm-3"></div>
      <div class="col-sm-7 center-block">
        <button type="submit" class="btn btn-default" id="save">Save Changes</button>
        <button class="btn btn-danger" type="submit" id="delete">Delete Project</button>
      </div>
    <div class="col-sm-2"></div>
</form>
</div>
<div class="col-sm-2"></div>
</div>
</c:forEach>
</c:if>
 
<div class="row portfolio-form-container" id="formNewPortfolio">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
  
  <form method="POST" action="${formAction}" id="newProjectForm">
 	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}" />
    <input type="hidden" name="studentId" value="${student.studentId}" />
    <input type="hidden" name="userId" value="${student.userId}" />
	<input type="hidden" name="portfolioId" value="0"/>
	
	<!-- Input fields -->
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="projectName" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Project</button></label>
      </span>
      <input type="text" class="form-control" name="projectName" maxlength="100">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="description" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Description</button></label>
      </span>
      <input type="text" class="form-control" name="description" maxlength="2000">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn edit">
        <label for="link" class="col-sm-2 control-label"><button class="btn btn-default" type="button">Link</button></label>
      </span>
      <input type="text" class="form-control" name="link" maxlength="100">
      <span class="glyphicon glyphicon-ok form-control-feedback has-feedback"></span>
    </div>
    
    <!-- Submit Button -->
    <div class="col-sm-3"></div>
      <div class="col-sm-7 center-block">
        <button type="submit" class="btn btn-default" id="save">Save Changes</button>
      </div>
    <div class="col-sm-2"></div>
</form>
</div>
<div class="col-sm-2"></div>
</div>
        
<c:url var="jsHref" value="/js/validatePortfolio.js"/>  
<script src="${jsHref}"></script>        
<c:import url="/WEB-INF/jsp/footer.jsp" />