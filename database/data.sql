-- *****************************************************************************
-- This script contains INSERT statements for populating tables with seed data
-- *****************************************************************************

BEGIN;

INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'gsheeley','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'mpollins','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'ckoebel','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (2,'jdoe','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (0,'twarnock','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'tsayre','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'rsapp','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'jsuen','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'carmstrong','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'kwilliams','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'hstork','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'jwilson','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'rbecker','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'nanderson','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'jvorous','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'droig','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'nlewis','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'mcarlson','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'phedman','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'dblankfield','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'corr','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'ashafron','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'rhurd','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'dsullivan','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'avalentino','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'lrwilliams','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'candrikanich','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);
INSERT INTO appuser (permission,userName,password,salt,accountStatus) VALUES (1,'sstar','ok9dtIa6aGPbpEe36tFjlA==','xQo21f7svBboVTnSQx6wH4Iq9p2+8wcbmUq6fstEA6Bl+TD/VP3aoTOsumPcbjQAYJZ9rmEXV5hALy1Hq20mGcbEM0IQVAeoinxtHcMX9mhUMkuJ/xBdOIRoqViVsfk28e7WD1bQfgIImvRzLhNJR0GA1Q3BcuWfSdxtEzAo7N8=',true);

INSERT INTO contact (contactPref,email,phone,address1,city,state,zip) VALUES ('email','Iammcgaber@gmail.com','614-446-9211','5430 North Meadows Blvd','Columbus','Ohio','43229');
INSERT INTO contact (contactPref,email) VALUES ('email','mpollins@gmail.com');
INSERT INTO contact (contactPref,email,phone) VALUES ('email','carolinekoebel@gmail.com','512-650-5148');
INSERT INTO contact (contactPref,email) VALUES ('email','jdoe@rain.com');
INSERT INTO contact (contactPref,email) VALUES ('email','twarnock@techelevator.com');
INSERT INTO contact (contactPref,email) VALUES ('email','rtylersayre@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','sappryam@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','jason.suen01@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','christian@medpactech.com');
INSERT INTO contact (contactPref,email) VALUES ('email','speakwilliams@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','hilarys54@aol.com');
INSERT INTO contact (contactPref,email) VALUES ('email','Jordanw045@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','rbex216@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','nathanaelcanderson@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','Vorousja@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','baduche@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','nlewis86@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','mattcarlson.3226@gmailcom');
INSERT INTO contact (contactPref,email) VALUES ('email','peterhedman@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','db313706@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','Callumrorr@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','aaronshafron@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','rhhurd@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','drew.sullivan.dma@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','avalentino47@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','mathletech7@gmail.com');
INSERT INTO contact (contactPref,email) VALUES ('email','chris@andrikanich.com');
INSERT INTO contact (contactPref,email) VALUES ('email','sam.stars.internet.monik@gmail.com');

INSERT INTO cohort (location,programmingLang,cohortNumber,isCurrent) VALUES ('Columbus','Java','0',true);
INSERT INTO cohort (location,programmingLang,cohortNumber,isCurrent) VALUES ('Cleveland','Java','0',false);
INSERT INTO cohort (location,programmingLang,cohortNumber,isCurrent) VALUES ('Cleveland','Java','1',false);
INSERT INTO cohort (location,programmingLang,cohortNumber,isCurrent) VALUES ('Cleveland','.NET','1',false);
INSERT INTO cohort (location,programmingLang,cohortNumber,isCurrent) VALUES ('Cleveland','Java','2',true);
INSERT INTO cohort (location,programmingLang,cohortNumber,isCurrent) VALUES ('Cleveland','.NET','2',false);
		
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable,summary,interests) VALUES (1,1,1,'Gabriel','Sheeley',true,'Proven to excel in diverse environments, on a team as well as independently, by leveraging problem solving skills, attention to detail, and self-motivation.','Arduino, rock climbing, caving, hiking, family, traveling, spear fishing, free-diving, Columbus JavaScript User group');
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable,summary) VALUES (2,2,1,'Michael','Pollins',true,'I am a passionate puzzle solver, strategic thinker, and articulate communicator. I make decisions backed by data and detailed analysis. I am looking forward to a career using coding and software to solve problems.');
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable,summary) VALUES (3,3,1,'Caroline','Koebel',true,'Transitioning into software development and the tech field from a foundation as a film-video director and writer with international exposure and extensive experience in the arts and education. Akin to art making and writing, coding bridges relations between the theoretical and the practical. I seek a position as a junior software developer or technical consultant in order to maximize the potential of this bridge between thought and form.');
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (6,6,1,'Tyler','Sayre',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (7,7,1,'Ryan','Sapp',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (8,8,1,'Jason','Suen',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (9,9,1,'Christian','Armstrong',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (10,10,1,'Keith','Williams',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (11,11,1,'Hilary','Stork',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (12,12,1,'Jordan','Wilson',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (13,13,1,'Richard','Becker',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (14,14,1,'Nathanael','Anderson',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (15,15,5,'James','Vorous',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (16,16,5,'Del','Roig',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (17,17,5,'Nick','Lewis',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (18,18,5,'Matthew','Carlson',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (19,19,5,'Peter','Hedman',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (20,20,5,'Dave','Blankfield',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (21,21,5,'Callum','Orr',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (22,22,5,'Aaron','Shafron',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (23,23,5,'Rebecca','Hurd',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (24,24,5,'Drew','Sullivan',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (25,25,5,'Anthony','Valentino',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (26,26,5,'LeBreon Ray','Williams',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (27,27,5,'Chris','Andrikanich',true);
INSERT INTO student (userId,contactId,cohortId,firstName,lastName,isDiscoverable) VALUES (28,28,5,'Sam','Star',true);
	
INSERT INTO employer (userId,contactId,businessName,firstName,lastName,description,notes) VALUES (4,4,'J Doe Rain Tech','J','Doe','Software startup specializing in ensuring clients never get wet again.','Seeking developers who are passionate about creating wetness-resisting apps.');

INSERT INTO staff (userId,contactId,firstName,lastName) VALUES (5,5,'Terry','Warnock');

INSERT INTO social_media (name) VALUES ('linkedIn'); 
INSERT INTO social_media (name) VALUES ('web');
INSERT INTO social_media (name) VALUES ('twitter');
INSERT INTO social_media (name) VALUES ('facebook');

INSERT INTO student_social_media (studentId,socialMediaId,link) VALUES (1,1,'https://www.linkedin.com/in/gabrielsheeley');
INSERT INTO student_social_media (studentId,socialMediaId,link) VALUES (1,2,'http://pillartechnology.com/');
INSERT INTO student_social_media (studentId,socialMediaId,link) VALUES (1,3,'https://www.twitter.com/');
INSERT INTO student_social_media (studentId,socialMediaId,link) VALUES (1,4,'http://facebook.com/');
	
INSERT INTO tech_skills (techSkillName) VALUES ('Java');
INSERT INTO tech_skills (techSkillName) VALUES ('SQL');
INSERT INTO tech_skills (techSkillName) VALUES ('.NET');
INSERT INTO tech_skills (techSkillName) VALUES ('TDD');
INSERT INTO tech_skills (techSkillName) VALUES ('JUnit');
INSERT INTO tech_skills (techSkillName) VALUES ('Maven');
INSERT INTO tech_skills (techSkillName) VALUES ('Spring');
INSERT INTO tech_skills (techSkillName) VALUES ('JavaScript');
INSERT INTO tech_skills (techSkillName) VALUES ('HTML');
INSERT INTO tech_skills (techSkillName) VALUES ('CSS');
INSERT INTO tech_skills (techSkillName) VALUES ('Servlets');
INSERT INTO tech_skills (techSkillName) VALUES ('JSP');
INSERT INTO tech_skills (techSkillName) VALUES ('Spring Web MVC');
INSERT INTO tech_skills (techSkillName) VALUES ('PostgreSQL');
INSERT INTO tech_skills (techSkillName) VALUES ('JDBC');
INSERT INTO tech_skills (techSkillName) VALUES ('Git');
INSERT INTO tech_skills (techSkillName) VALUES ('Eclipse');
INSERT INTO tech_skills (techSkillName) VALUES ('Web MVC');
INSERT INTO tech_skills (techSkillName) VALUES ('Tomcat');
INSERT INTO tech_skills (techSkillName) VALUES ('UML');
INSERT INTO tech_skills (techSkillName) VALUES ('ER Diagram');

INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,1);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,2);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,3);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,4);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,5);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,6);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,7);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,8);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,9);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,10);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,11);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,12);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,13);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,14);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,15);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,16);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,17);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,18);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,19);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,20);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (1,21);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (2,1);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (2,2);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (3,3);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (3,4);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (4,5);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (4,6);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (5,7);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (5,8);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (6,9);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (6,10);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (7,11);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (7,12);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (8,13);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (8,14);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (9,15);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (9,16);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (10,17);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (10,18);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (11,19);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (11,20);
INSERT INTO student_tech_skills (studentId,techSkillId) VALUES (12,21);


INSERT INTO soft_skills (softSkillName) VALUES ('Communication');
INSERT INTO soft_skills (softSkillName) VALUES ('Leadership');
INSERT INTO soft_skills (softSkillName) VALUES ('Collaboration');
INSERT INTO soft_skills (softSkillName) VALUES ('Organization');
INSERT INTO soft_skills (softSkillName) VALUES ('Problem solving');
INSERT INTO soft_skills (softSkillName) VALUES ('Critical Thinker');
INSERT INTO soft_skills (softSkillName) VALUES ('Imaginative');;

INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (1,1);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (1,2);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (1,3);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (1,4);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (1,5);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (1,6);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (1,7);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (2,1);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (2,2);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (3,3);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (3,4);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (4,5);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (4,6);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (5,7);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (5,1);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (6,2);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (6,3);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (7,4);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (7,5);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (8,6);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (8,7);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (9,1);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (9,2);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (10,3);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (10,4);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (11,5);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (11,6);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (12,7);
INSERT INTO student_soft_skills (studentId,softSkillId) VALUES (12,1);

INSERT INTO experience (studentId,company,title,startMonth,startYear,description) VALUES (1,'Tech Elevator','Full Stack Web Developer','September','2016','Tech Elevator is a 14 week Java coding bootcamp and in-person educational platform focused on developing dynamic web based software systems using the Java programming language and platform including:
• Programming Fundamentals: Java, OOP, Data Structures, Agile, Eclipse, Maven, Dependency Injection (Spring)
• Java Language Fundamentals: variables, data types, loops, conditional statements, exception handling, collections framework, I/O
• Object Oriented Programming: classes and objects, interfaces, access modifiers, packages, the Java classpath, class modeling, encapsulation, inheritance, and polymorphism, UML class diagrams
• Web Application Development: HTML, CSS, JavaScript, Servlets, JSP, Spring Web MVC, Tomcat, Rest, JSON, Twitter Bootstrap, Client-side validation, jQuery
• Database Programming: SQL, PostgreSQL, JDBC, Spring JDBC, table design and creation, E/R diagrams
• Server-Side Development: TCP/IP, HTTP, DNS, Sessions, MVC (Spring WebMVC), Request mapping, Server-side validation
• Application Security: SQL Injection, Cross-site scripting, Encryption
• Development tools and techniques: TDD, Unit testing (JUnit), Integration testing, Acceptance Testing (Selenium, Cucumber), unix command line navigation, Git, Eclipse');
INSERT INTO experience (studentId,company,title,startYear,description) VALUES (1,'Self-employed','Web Developer','2012','Since 2012 I have been teaching myself web development through online resources, books, seminars, and good old fashion practice. I have stayed away from production work, mostly doing personal projects or projects for friends. But these years of experience have given me the knowledge necessary to learn quickly in any development environment.');
INSERT INTO experience (studentId,company,title,startMonth,startYear,endMonth,endYear,description) VALUES (1,'All In Wood Construction','Carpenter','September','2012','December','2014','The experienced master carpenters and craftsmen of All In Wood Construction have been creating beautiful living spaces for decades. Their in-house shop provides custom millwork and cabinetry to match virtually any existing style, or a totally new design to compliment your personal style and taste.');

INSERT INTO education (studentId,school,startYear,endYear,degree,fieldOfStudy,description) VALUES (1,'Columbus State Community College','2015','2018','2','Computer Software Engineering','As a Computer Science major at Columbus state I am focusing on Software Development, which includes classes in Java, JavaScript, programming fundamentals, HTML/CSS, logic, business management, SQL, systems analysis, management, database fundamentals, and networking. Being a full time employee, a husband and father of two children, and maintaining a 3.75 GPA while taking 11 credit hours is a testimony to my dedication, willingness to work hard, and desire to learn and grow.');
INSERT INTO education (studentId,school,startYear,endYear,degree,fieldOfStudy,description) VALUES (1,'Tech Elevator','2016','2016', '1','Java Developer, Computer Science','Tech Elevator is a 14 week Java coding bootcamp and in-person educational platform focused on developing dynamic web based software systems using the Java programming language and platform including:
• Java Language Fundamentals: variables, data types, loops, conditional statements, exception handling, collections framework, I/O
• Object Oriented Programming: classes and objects, interfaces, access modifiers, packages, the Java classpath, class modeling, encapsulation, inheritance, and polymorphism, UML class diagrams
• Web Application Development: HTML, CSS, JavaScript, Servlets, JSP, Spring Web MVC, Tomcat
• Database Programming: PostgreSQL, JDBC, table design and creation, E/R diagrams
• Development tools and techniques: unit testing (JUnit), TDD, unix command line navigation, Git, Eclipse');

INSERT INTO portfolio (studentId,projectName,description,link) VALUES (1,'Arduino Odyssey','Totally rad programming and robotics play conducted during the wee hours of the night to be revealed at CodeMash 2017.','http://www.scottsbots.com/');
INSERT INTO portfolio (studentId,projectName,description,link) VALUES (1,'Ruby Umbrella Screen','Coding, nature and hardware come together in highly imaginative ways.','http://christojeanneclaude.net/');

INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,1);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,2);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,3);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,4);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,5);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,6);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,7);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,8);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,9);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (1,10);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,11);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,12);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,13);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,14);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,15);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,16);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,17);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,18);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,19);
INSERT INTO portfolio_tech (portfolioId,techSkillId) VALUES (2,20);

COMMIT;