-- *************************************************************************************************
-- This script creates all of the database objects (tables, sequences, etc) for the database
-- *************************************************************************************************

BEGIN;
CREATE TABLE student
(
	studentId serial NOT NULL,
	userId integer,
	contactId integer,
	cohortId integer,
	firstName varchar(25),
	lastName varchar(25),
	isDiscoverable boolean DEFAULT false,
	summary varchar(500),
	interests varchar(250),
	CONSTRAINT pk_student_studentId PRIMARY KEY (studentId)
);

CREATE TABLE cohort
(
	cohortId serial NOT NULL,
	location varchar(25), 
	programmingLang varchar(10),
	cohortNumber varchar(10),
	isCurrent boolean DEFAULT true,
	CONSTRAINT pk_cohort_cohortId PRIMARY KEY (cohortId)
);

CREATE TABLE experience
(
	experienceId serial NOT NULL,
	studentId integer,
	company varchar(50),
	title varchar(50),
	startMonth varchar(10),
	startYear varchar(10),
	endMonth varchar(10),
	endYear varchar(10),
	description varchar(2000),
	CONSTRAINT pk_experience_experienceId PRIMARY KEY (experienceId)
);

CREATE TABLE education
(
	educationId serial NOT NULL,
	studentId integer,
	school varchar(50),
	startMonth varchar(10),
	startYear varchar(10),
	endMonth varchar(10),
	endYear varchar(10),
	degree varchar(50),
	fieldOfStudy varchar(50),
	description varchar(2000),
	CONSTRAINT pk_education_educationId PRIMARY KEY (educationId)
);

CREATE TABLE tech_skills
(
	techSkillId serial NOT NULL,
	techSkillName varchar(25),
	CONSTRAINT pk_tech_skills_techSkillId PRIMARY KEY (techSkillId)
);

CREATE TABLE soft_skills
(
	softSkillId serial NOT NULL,
	softSkillName varchar(25),
	CONSTRAINT pk_soft_skills_softSkillId PRIMARY KEY (softSkillId)
);

CREATE TABLE student_tech_skills
(
	studentId integer NOT NULL,
	techSkillId integer NOT NULL,
	CONSTRAINT pk_student_tech_skills_studentId_techSkillId PRIMARY KEY (studentId, techSkillId)
);

CREATE TABLE student_soft_skills
(
	studentId integer NOT NULL,
	softSkillId integer NOT NULL,
	CONSTRAINT pk_student_soft_skills_studentId_softSkillId PRIMARY KEY (studentId, softSkillId)
);

CREATE TABLE social_media
(
	socialMediaId serial NOT NULL,
	name varchar(25),
	CONSTRAINT pk_social_media_socialMediaId PRIMARY KEY (socialMediaId)
);

CREATE TABLE student_social_media
(
	studentId integer NOT NULL,
	socialMediaId integer NOT NULL,
	link varchar(100),
	CONSTRAINT pk_student_social_media_studentId_socialMediaId PRIMARY KEY (studentId, socialMediaId)
);

CREATE TABLE portfolio
(
	portfolioId serial NOT NULL,
	studentId integer,
	projectName varchar(100),
	description varchar(2000),
	link varchar(100),
	CONSTRAINT pk_portfolio_portfolioId PRIMARY KEY (portfolioId)
);

CREATE TABLE portfolio_tech
(
	portfolioId integer NOT NULL,
	techSkillId integer NOT NULL,
	CONSTRAINT pk_portfolio_tech_portfolioId_techSkillId PRIMARY KEY (portfolioId, techSkillId)
);

CREATE TABLE staff (
	staffId serial NOT NULL,
	userId integer,
	contactId integer,
	firstName varchar(25),
	lastName varchar(25),
	CONSTRAINT pk_staff_staffId PRIMARY KEY (staffId)
);

CREATE TABLE appuser (
	userId serial NOT NULL,
	permission integer,
	userName varchar(25),
	password varchar(50),
	salt varchar(300),
	accountStatus boolean DEFAULT false,
	CONSTRAINT pk_appuser_userId PRIMARY KEY (userId),
	CONSTRAINT appuser_unique UNIQUE (userName)
);

CREATE TABLE contact (
	contactId serial NOT NULL,
	contactPref varchar(25) DEFAULT 'email',
	email varchar(50) NOT NULL,
	phone varchar(25),
	address1 varchar(50),
	address2 varchar(50),
	city varchar(25),
	state varchar(15),
	zip varchar(15),
	CONSTRAINT pk_contact_contactId PRIMARY KEY (contactId)
);

CREATE TABLE employer (
	employerId serial NOT NULL,
	userId integer,
	contactId integer,
	businessName varchar(50),
	firstName varchar(25),
	lastName varchar(25),
	description varchar(2000),
	notes varchar(250),
	CONSTRAINT pk_employer_employerId PRIMARY KEY (employerId)
);


ALTER TABLE student
ADD FOREIGN KEY(userId)
REFERENCES appuser(userId);

ALTER TABLE student
ADD FOREIGN KEY(contactId)
REFERENCES contact(contactId);

ALTER TABLE student
ADD FOREIGN KEY(cohortId)
REFERENCES cohort(cohortId);

ALTER TABLE portfolio
ADD FOREIGN KEY (studentId)
REFERENCES student(studentId);

ALTER TABLE student_tech_skills
ADD FOREIGN KEY(studentId)
REFERENCES student(studentId);

ALTER TABLE student_tech_skills
ADD FOREIGN KEY(techSkillId)
REFERENCES tech_skills(techSkillId);

ALTER TABLE student_soft_skills
ADD FOREIGN KEY(studentId)
REFERENCES student(studentId);

ALTER TABLE student_soft_skills
ADD FOREIGN KEY(softSkillId)
REFERENCES soft_skills(softSkillId);

ALTER TABLE portfolio_tech
ADD FOREIGN KEY(portfolioId)
REFERENCES portfolio(portfolioId);

ALTER TABLE portfolio_tech
ADD FOREIGN KEY(techSkillId)
REFERENCES tech_skills(techSkillId);

ALTER TABLE student_social_media
ADD FOREIGN KEY(studentId)
REFERENCES student(studentId);

ALTER TABLE student_social_media
ADD FOREIGN KEY(socialMediaId)
REFERENCES social_media(socialMediaId);

ALTER TABLE staff
ADD FOREIGN KEY(userId)
REFERENCES appuser(userId);

ALTER TABLE staff
ADD FOREIGN KEY(contactId)
REFERENCES contact(contactId);

ALTER TABLE employer
ADD FOREIGN KEY(userId)
REFERENCES appuser(userId);

ALTER TABLE employer
ADD FOREIGN KEY(contactId)
REFERENCES contact(contactId);

ALTER TABLE experience
ADD FOREIGN KEY(studentId)
REFERENCES student(StudentId);

ALTER TABLE education
ADD FOREIGN KEY(studentId)
REFERENCES student(StudentId);

ALTER TABLE portfolio
ADD FOREIGN KEY(studentId)
REFERENCES student(studentId);

COMMIT;