CREATE TABLE student
(
	studentId serial NOT NULL,
	userId integer NOT NULL,
	contactId integer NOT NULL,
	firstName varchar(50) NOT NULL,
	lastName varchar(50) NOT NULL,
	cohort varchar(50) NOT NULL, 
	programmingLang varchar(25) NOT NULL,
	isCurrent boolean DEFAULT true NOT NULL,
	address1 varchar(50),
	address2 varchar(50),
	city varchar(50),
	state varchar(50),
	zip varchar(50),
	linkedIn varchar(50),
	web varchar(50),
	summary varchar(250),
	experience varchar(500),
	education varchar(500),
	interests varchar(250),
	CONSTRAINT pk_student_studentId PRIMARY KEY (studentId)
);

CREATE TABLE tech_skills
(
	techSkillId serial NOT NULL,
	techSkillName varchar(25) NOT NULL,
	CONSTRAINT pk_tech_skills_techSkillId PRIMARY KEY (techSkillId)
);

CREATE TABLE soft_skills
(
	softSkillId serial NOT NULL,
	softSkillName varchar(25) NOT NULL,
	CONSTRAINT pk_soft_skills_softSkillId PRIMARY KEY (softSkillId)
);

CREATE TABLE student_tech_skills
(
	studentId integer NOT NULL,
	techSkillId integer NOT NULL,
	CONSTRAINT pk_student_tech_skills_studentId_techSkillId PRIMARY KEY (studentId_techSkillId)
);

CREATE TABLE student_soft_skills
(
	studentId integer NOT NULL,
	softSkillId integer NOT NULL,
	CONSTRAINT pk_student_soft_skills_studentId_softSkillId PRIMARY KEY (studentId_softSkillId)
);

CREATE TABLE staff (
	staffId serial NOT NULL,
	userId integer NOT NULL,
	contactId integer NOT NULL,
	firstName varchar(50) NOT NULL,
	lastName varchar(50) NOT NULL,
	CONSTRAINT pk_staff_staffId PRIMARY KEY (staffId)
);

CREATE TABLE user (
	userId serial NOT NULL,
	userName varchar(50) NOT NULL,
	password varchar(50) NOT NULL,
	CONSTRAINT pk_user_userId PRIMARY KEY (userId)
);

CREATE TABLE contact (
	contactId serial NOT NULL,
	contactPref varchar(25) NOT NULL,
	email varchar(50),
	phone varchar(25),
	CONSTRAINT pk_contact_contactId PRIMARY KEY (contactId)
);

CREATE TABLE employer (
	employerId serial NOT NULL,
	userId integer NOT NULL,
	contactId integer NOT NULL,
	businessName varchar(50) NOT NULL,
	firstName varchar(50) NOT NULL,
	lastName varchar(50) NOT NULL,
	linkedIn varchar(50),
	web varchar(50),
	description varchar(250),
	notes varchar(250),
	CONSTRAINT pk_employer_employerId PRIMARY KEY (employerId)
);

ALTER TABLE student
ADD FOREIGN KEY(userId)
REFERENCES user(userId);

ALTER TABLE student
ADD FOREIGN KEY(contactId)
REFERENCES contact(contactId);

ALTER TABLE student_tech_skills
ADD FOREIGN KEY(studentId)
REFERENCES student(studentId);

ALTER TABLE student_tech_skills
ADD FOREIGN KEY(techSkillId)
REFERENCES tech_skills(techSkillId);

ALTER TABLE student_soft_skills
ADD FOREIGN KEY(studentId)
REFERENCES student(studentId);

ALTER TABLE student_soft_skills
ADD FOREIGN KEY(techSkillId)
REFERENCES tech_skills(techSkillId);

ALTER TABLE staff
ADD FOREIGN KEY(userId)
REFERENCES user(userId);

ALTER TABLE staff
ADD FOREIGN KEY(contactId)
REFERENCES contact(contactId);

ALTER TABLE employer
ADD FOREIGN KEY(userId)
REFERENCES user(userId);

ALTER TABLE employer
ADD FOREIGN KEY(contactId)
REFERENCES contact(contactId);
	



INSERT INTO student (firstName,lastName,summary) VALUES ('Gabriel','Sheeley','Proven to excel in diverse environments, on a team as well as independently, by leveraging problem solving skills, attention to detail, and self-motivation. Embodies a strong desire to succeed by providing exceptional results and value to customers. Transitioning to software development to use these skills and passions to build elegant software that solves complex problems.');

INSERT INTO student (firstName,lastName,summary) VALUES ('Michael','Pollins','I am a passionate puzzle solver, strategic thinker, and articulate communicator. I make decisions backed by data and detailed analysis. I am looking forward to a career using coding and software to solve problems.');

INSERT INTO student (firstName,lastName,summary) VALUES ('Caroline','Koebel','Transitioning into software development and the tech field from a foundation as a film-video director and writer with international exposure and extensive experience in the arts and education. Akin to art making and writing, coding bridges relations between the theoretical and the practical. I seek a position as a junior software developer or technical consultant in order to maximize the potential of this bridge between thought and form.');



