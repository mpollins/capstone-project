CREATE TABLE student
(
firstName varchar(64),
lastName varchar(64),
summary varchar(500)
);

ALTER TABLE student ADD experience varchar(1000);
ALTER TABLE student ADD education varchar(500);


INSERT INTO student (firstName,lastName,summary)
VALUES ('Gabriel','Sheeley','Proven to excel in diverse environments, on a team as well as independently, by leveraging problem solving skills, attention to detail, and self-motivation. Embodies a strong desire to succeed by providing exceptional results and value to customers. Transitioning to software development to use these skills and passions to build elegant software that solves complex problems.');

INSERT INTO student (firstName,lastName,summary) VALUES ('Michael','Pollins','I am a passionate puzzle solver, strategic thinker, and articulate communicator. I make decisions backed by data and detailed analysis. I am looking forward to a career using coding and software to solve problems.');

INSERT INTO student (firstName,lastName,summary) VALUES ('Caroline','Koebel','Transitioning into software development and the tech field from a foundation as a film-video director and writer with international exposure and extensive experience in the arts and education. Akin to art making and writing, coding bridges relations between the theoretical and the practical. I seek a position as a junior software developer or technical consultant in order to maximize the potential of this bridge between thought and form.');

